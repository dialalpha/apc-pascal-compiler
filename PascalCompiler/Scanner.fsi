﻿// Signature file for Scanner module

[<RequireQualifiedAccess>]
module Scanner

/// principal function driving the Scanner
val scan: string -> System.Collections.Generic.List<PascalTokens.Token>
