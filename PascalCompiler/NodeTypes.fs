﻿module NodeTypes

open PascalTokens
open Attributes

type ASTNode = {
    nodeType : NodeTypes
    value : option<Token>
    children : ResizeArray<ASTNode>
    mutable linkToDeclaration : ASTNode option
    mutable attribute : Attribute option
    mutable address : (int * int) option
    }
    with
        member this.printNode () =
            printf "%A\t" this.nodeType
            match this.value with
            |Some n -> printf "%A\t" n
            |None -> ()
            match this.attribute with
            |Some a -> printf "%A" a
            |_ -> ()
    end

and NodeTypes =
    |Actual_Function
    |Actual_Parameter
    |Actural_Parameter_List
    |Actual_Procedure
    |Actual_Value
    |Actual_Variable
    |Addition_Operator
    |Array_Assignment_Statement
    |Array_Type
    |Array_Variable
    |Assignment_Statement
    |Base_Type
    |Block
    |Bound_Identifier
    |Bound_Specification
    |Case_Label_List
    |Case_Limb
    |Case_Statement
    |Component_Variable
    |Compound_Statement
    |Conditional_Statement
    |Conformant_Array_Schema
    |Constant
    |Constant_Definition
    |Constant_Definition_Part
    |Constant_Identifier
    |Declaration_Part
    |Digit
    |Digit_Sequence
    |Default
    |Directive
    |Element_List
    |Element_Type
    |Entire_Variable
    |Enumerated_Type
    |Expression
    |Expression_List
    |Factor
    |Field_Designator
    |Field_List
    |Field_Width
    |File_Buffer
    |File_Component_Type
    |File_Type
    |File_Variable
    |Final_Expression
    |Fixed_Part
    |For_Statement
    |Formal_Parameter_List
    |Formal_Parameter_Section
    |Fraction_Length
    |Function_Body
    |Function_Declaration
    |Function_Designator
    |Function_Heading
    |Function_Identification
    |Function_Identifier
    |Function_Parameter_Section
    |Goto_Statement
    |Identifier
    |Identifier_List
    |If_Statement
    |Indexed_Type
    |Indexed_Variable
    |Initial_Expression
    |Integer_Number
    |Label
    |Label_Declaration_Part
    |Letter
    |Local_Block
    |Local_Declaration_Part
    |Lower_Bound
    |Multiplication_Operator
    |Number
    |Ordinal_Type_Identifier
    |Output_List
    |Output_Value
    |Packed_Conformant_Array_Schema
    |Parameter_Type
    |Pointer_Type
    |Pointer_Variable
    |Procedure_And_Function_Declaration_Part
    |Procedure_Body
    |Procedure_Declaration
    |Procedure_Heading
    |Procedure_Identification
    |Procedure_Identifier
    |Procedure_Parameter_Section
    |Procedure_Statement
    |Program
    |Program_Heading
    |Real_Number
    |Record_Section
    |Record_Type
    |Record_Variable
    |Referenced_Variable
    |Relational_Operator
    |Repeat_Statement
    |Repetitive_Statement
    |Required_Type
    |Result_Type
    |Scale_Factor
    |Set
    |Set_Type
    |Sign
    |Simple_Expression
    |Simple_Statement
    |Statement
    |Statement_Part
    |Statement_Sequence
    |String
    |String_Character
    |Structured_Statement
    |Structured_Type
    |Subrange_Type
    |System_Procedure_Identifier
    |Tag_Field
    |Term
    |Type
    |Type_Defintion
    |Type_Definition_Part
    |Type_Identifier
    |Unpacked_Conformant_Array_Schema
    |Unpacked_Structured_Type
    |Unsigned_Digit_Sequence
    |Upper_Bound
    |Value_Parameter_Section
    |Var_Assignment_Statement
    |Variable
    |Variable_Declaration
    |Variable_Declaration_Part
    |Variable_Identifier
    |Variable_List
    |Variable_Parameter_Section
    |Variant
    |Variant_Part
    |While_Statement
    |With_Statement
    |KW //keyword
