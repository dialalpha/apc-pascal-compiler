﻿module Parser

open System
open PascalTokens
open NodeTypes

let defaultNode = {
    nodeType = NodeTypes.Default
    value = None
    children = ResizeArray<ASTNode>()
    linkToDeclaration = None
    attribute = None
    address = None
}

/// Main data strcture driving the parser
type ParserDriver = {
    tokenList : ResizeArray<Token>
    mutable pos : int
    mutable curToken : Token
    mutable parseError : bool
    mutable errorMessage : string Option
    }
    with 
        member this.fetchNextToken () =
            if this.pos < (this.tokenList.Count - 2) then
                this.pos <- this.pos + 1
                this.curToken <- this.tokenList.[this.pos]
            else this.curToken <- this.tokenList.[this.tokenList.Count - 1]
            
        member this.peek () =
            if this.pos < (this.tokenList.Count - 2) then
                this.tokenList.[this.pos + 1]
            else Token.TK_EOF
        
        member this.setError (str) = 
            this.parseError <- true 
            this.errorMessage <- Some str
    end

//===== Small helper funcs =============//
let isSemiColon token =
    match token with
    |Token.SYM_SEMICOLON(_) -> true
    |_ -> false

let isComma token =
    match token with
    |Token.SYM_COMMA(_) -> true
    |_ -> false    

let isIdentifier token =
    match token with
    |Token.TK_IDENT(_) -> true
    |_ -> false

 //============= Parsing functions =================//

/// Parse Identifier
let rec parse_Identifier (parser : ParserDriver) =
    let idNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Identifier; value = Some parser.curToken}
    parser.fetchNextToken ()
    idNode

/// Parse Constant
and parse_Constant (parser : ParserDriver) =
    let constNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Constant}
    if not parser.parseError then
        match parser.curToken with
        |Token.OP_SUB(_) |Token.OP_ADD(_) -> 
            constNode.children.Add(parse_Sign parser)
            match parser.curToken with
            |Token.TK_IDENT(_) -> constNode.children.Add(parse_Identifier parser)
            |Token.TK_REALLIT(_) |Token.TK_INTLIT(_) -> constNode.children.Add(parse_Number parser)
            |_ -> parser.setError("Parser Error: Expected a number literal or identifier")
        |Token.TK_IDENT(_) -> constNode.children.Add(parse_Identifier parser)
        |Token.TK_REALLIT(_) |Token.TK_INTLIT(_) -> constNode.children.Add(parse_Number parser)
        |Token.TK_STRLIT(_) -> constNode.children.Add(parse_String parser)
        |Token.KW_TRUE(_) |Token.KW_FALSE(_) |Token.KW_NIL(_) ->
            let KWNode = {
                nodeType = NodeTypes.KW
                value = Some parser.curToken
                children = ResizeArray<ASTNode>(0)
                attribute = None
                linkToDeclaration = None
                address = None
            }
            constNode.children.Add(KWNode)
            parser.fetchNextToken ()
        |_ -> parser.setError("Parser Error: Unexpected token while parsing CONSTANT.")
    //return
    constNode

/// Parse String literal
and parse_String parser =
    let strNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.String; value = Some parser.curToken}
    parser.fetchNextToken () 
    //return
    strNode

/// Parse Number
and parse_Number (parser : ParserDriver) =
    let numNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Number; value = Some parser.curToken}
    parser.fetchNextToken ()
    numNode

/// Parse sign
and parse_Sign (parser : ParserDriver) =
    let signNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Sign; value = Some parser.curToken}
    parser.fetchNextToken ()
    // return
    signNode

/// Parse +, -, or, etc...
let parse_AdditionOperator (parser : ParserDriver) =
    let addOpNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Addition_Operator; value = Some parser.curToken}
    parser.fetchNextToken ()
    addOpNode

/// Parse *, /, div, mod, and, etc..
let parse_MultiplicationOperator (parser : ParserDriver) =
    let multOpNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Multiplication_Operator; value = Some parser.curToken}
    parser.fetchNextToken ()
    multOpNode

/// Parse <, >, <=, >=, <>
let parse_RelationalOperator (parser : ParserDriver) =
    let relOpNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Relational_Operator; value = Some parser.curToken}
    parser.fetchNextToken ()
    relOpNode

/// Parse Factor
let rec parse_Factor (parser : ParserDriver) =
    let factorNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Factor}
    if not parser.parseError then
        match parser.curToken with
        |Token.OP_NOT(_) -> 
            let notNode = {
                nodeType = NodeTypes.KW
                value = Some parser.curToken
                children = new ResizeArray<ASTNode>(0)
                linkToDeclaration = None
                attribute = None
                address = None
            }
            factorNode.children.Add(notNode)
            parser.fetchNextToken ()
            factorNode.children.Add(parse_Factor parser)
        |Token.KW_NIL(_) |Token.KW_TRUE(_) |Token.KW_FALSE(_)->
            let kwNode = {
                nodeType = NodeTypes.KW
                value = Some parser.curToken
                children = new ResizeArray<ASTNode>(0)
                linkToDeclaration = None
                attribute = None
                address = None
            }
            factorNode.children.Add(kwNode)
            parser.fetchNextToken ()
        |Token.TK_IDENT(_) -> 
            match parser.peek() with
            |Token.SYM_LBRACKET(_) -> factorNode.children.Add(parse_ArrayVariable parser)
            |_ -> factorNode.children.Add(parse_Identifier parser)
        |Token.TK_INTLIT(_) |Token.TK_REALLIT(_) -> factorNode.children.Add(parse_Number parser)
        |Token.TK_STRLIT(_) -> factorNode.children.Add(parse_String parser)
        |Token.SYM_LPAREN(_) ->
            parser.fetchNextToken ()
            factorNode.children.Add(parse_Expression parser)
            if not parser.parseError then
                match parser.curToken with
                |Token.SYM_RPAREN(_) -> parser.fetchNextToken ()
                |_ -> parser.setError ("Parser Error: Expecting right paren.")
        |_ -> 
            parser.setError ("Parser Error: No matching grammar rule for factor.")
    // return
    factorNode

/// Parse Term
and parse_Term (parser : ParserDriver) =
    let termNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Term}
    let isMultOp token =
        match token with
        |Token.OP_MULT(_) |Token.OP_REALDIV(_) |Token.OP_INTDIV(_)
        |Token.OP_INTMOD(_) |Token.OP_AND(_) -> true
        | _ -> false
    
    if not parser.parseError then
        termNode.children.Add(parse_Factor parser)
        while isMultOp parser.curToken && not parser.parseError do
            termNode.children.Add(parse_MultiplicationOperator parser)
            termNode.children.Add(parse_Factor parser)
    // return
    termNode

/// Parse Simple Expression
and parse_SimpleExpression (parser: ParserDriver) =
    let simpleExprNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Simple_Expression}
    let isAddOperator token =
        match token with
        |Token.OP_ADD(_) |Token.OP_SUB(_) |Token.OP_OR(_) -> true
        |_ -> false
    if not parser.parseError then
        match parser.curToken with
        |Token.OP_ADD(_) |Token.OP_SUB(_) ->
            simpleExprNode.children.Add(parse_Sign parser)
        | _ -> ()
        simpleExprNode.children.Add(parse_Term parser)
        while isAddOperator parser.curToken && not parser.parseError do 
            simpleExprNode.children.Add(parse_AdditionOperator parser)
            simpleExprNode.children.Add(parse_Term parser)
    // return
    simpleExprNode
    
/// Parse Expression
and parse_Expression (parser : ParserDriver) =
    let exprNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Expression}
    exprNode.children.Add(parse_SimpleExpression parser)
    if not parser.parseError then
        match parser.curToken with
        |Token.OP_EQUAL(_) |Token.OP_NOTEQUAL(_) |Token.OP_LESS(_) |Token.OP_LESSEQ(_)
        |Token.OP_GREAT(_) |Token.OP_GREATEQ(_) |Token.OP_IN(_) ->
            exprNode.children.Add(parse_RelationalOperator parser)
            if not parser.parseError then
                exprNode.children.Add(parse_SimpleExpression parser)
        | _ -> ()
    // return
    exprNode

/// Parse Array Variable
and parse_ArrayVariable parser =
    let arrayNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Array_Variable}
    arrayNode.children.Add(parse_Identifier parser) // parse array name
    parser.fetchNextToken () // eat lbracket
    arrayNode.children.Add(parse_Expression parser) // parse index
    if not parser.parseError then
        match parser.curToken with
        |Token.SYM_RBRACKET(_) -> parser.fetchNextToken ()
        |_ -> parser.setError("Parser Error: Unmatched bracked in array variable.")
    arrayNode

/// Parse Statement Sequence
let rec parse_StatementSequence (parser : ParserDriver) =
    let ssNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Statement_Sequence}
    if not parser.parseError then
        ssNode.children.Add(parse_Statement parser)
        while isSemiColon parser.curToken && not parser.parseError do
            parser.fetchNextToken ()
            ssNode.children.Add(parse_Statement parser)
    //return
    ssNode

/// Parse Statement
and parse_Statement (parser : ParserDriver) =
    let sNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Statement}
    let isAssignToken tok =
        match tok with
        |Token.OP_ASSIGN(_) -> true
        |_ -> false
    let isLBracketToken tok =
        match tok with
        |Token.SYM_LBRACKET(_) -> true
        |_ -> false
    if not parser.parseError then
        match parser.curToken with
        |Token.TK_IDENT(_) ->
            if isAssignToken (parser.peek()) then 
                sNode.children.Add(parse_VarAssignmentStatement parser)
            elif isLBracketToken (parser.peek()) then
                sNode.children.Add(parse_ArrayAssignmentStatement parser)
            else sNode.children.Add(parse_ProcedureStatement parser)
        |Token.KW_BEGIN(_) -> sNode.children.Add(parse_CompoundStatement parser)
        |Token.KW_WHILE(_) -> sNode.children.Add(parse_WhileStatement parser)
        |Token.KW_REPEAT(_) -> sNode.children.Add(parse_RepeatStatement parser)
        |Token.KW_FOR(_) -> sNode.children.Add(parse_ForStatement parser)
        |Token.KW_IF(_) -> sNode.children.Add(parse_IfStatement parser)
        |Token.KW_CASE(_) -> sNode.children.Add(parse_CaseStatement parser)
        |Token.KW_WITH(_) -> parser.setError("The _with_ statement is not supported")
        |Token.KW_GOTO(_) -> parser.setError("GOTO considered harmful.")
        |_ -> () // empty statement?
    //return
    sNode

/// Parse Assignment Statement
and parse_VarAssignmentStatement parser =
    let assignmentNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Var_Assignment_Statement} 
    assignmentNode.children.Add(parse_Identifier parser) //parse variable name
    parser.fetchNextToken () // eat assignment token
    if not parser.parseError then
        assignmentNode.children.Add(parse_Expression parser)
    //return
    assignmentNode

/// Parse Assignment to Array index
and parse_ArrayAssignmentStatement parser = 
    let assignmentNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Array_Assignment_Statement}  
    assignmentNode.children.Add(parse_ArrayVariable parser) // parse LHS
    if not parser.parseError then
        match parser.curToken with
        |Token.OP_ASSIGN(_) -> 
            parser.fetchNextToken () // eat assignment token
            assignmentNode.children.Add(parse_Expression parser) // parse RHS
        |_ -> parser.setError("Parser Error: Expecting ':=' in assignement statement.")
    // return
    assignmentNode

/// Parse Procedure Statement
and parse_ProcedureStatement parser =
    let procNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Procedure_Statement} 
    procNode.children.Add(parse_Identifier parser)  // match procedure name
    match parser.curToken with
    |Token.SYM_LPAREN(_) ->
        parser.fetchNextToken () // eat left paren
        procNode.children.Add(parse_Expression parser)
        while isComma parser.curToken && not parser.parseError do
            parser.fetchNextToken () // eat comma token
            procNode.children.Add(parse_Expression parser)
        if not parser.parseError then
            match parser.curToken with
            |Token.SYM_RPAREN(_) -> parser.fetchNextToken () // eat right paren
            |_ -> parser.setError("Parser Error: Expecting right prenthesis in procedure parameter list.")  
    |_ -> () // no procedure parameters -> do nothing
    //return
    procNode

/// Parse While Statement
and parse_WhileStatement parser =
    let whileNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.While_Statement} 
    if not parser.parseError then
        parser.fetchNextToken () // eat WHILE token
        whileNode.children.Add(parse_Expression parser)
        match parser.curToken with
        |Token.KW_DO(_) -> 
            parser.fetchNextToken()
            whileNode.children.Add(parse_Statement parser)
        |_ -> parser.setError("Parser error: Expecting keyword DO")
    //return
    whileNode

/// Parse Compound Statement
and parse_CompoundStatement parser =
    let compoundNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Compound_Statement} 
    if not parser.parseError then
        parser.fetchNextToken () // eat BEGIN keyword
        compoundNode.children.Add(parse_StatementSequence parser)
        match parser.curToken with
        |Token.KW_END(_) -> parser.fetchNextToken () // eat END keyword
        |_ -> parser.setError("Expected keyword: END.")
    //return
    compoundNode

/// Parse Repeat
and parse_RepeatStatement parser =
    let repeatNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Repeat_Statement} 
    if not parser.parseError then
        parser.fetchNextToken () // eat REPEAT token
        repeatNode.children.Add(parse_StatementSequence parser)
        if not parser.parseError then
            match parser.curToken with
            |Token.KW_UNTIL(_) ->
                parser.fetchNextToken () // eat UNTIL token
                repeatNode.children.Add(parse_Expression parser)
            |_ -> parser.setError("Parser Error: Expecting token UNTILL")
    //return
    repeatNode

/// Parse For Statement
and parse_ForStatement parser =
    let forNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.For_Statement} 
    if not parser.parseError then
        parser.fetchNextToken () // eat FOR token
        match parser.curToken with
        |Token.TK_IDENT(_) -> forNode.children.Add(parse_Identifier parser) // parse counter variable
        |_ -> parser.setError("Parser Error: Expecting a variable identifier in FOR statement.")
        if not parser.parseError then
            match parser.curToken with
            |Token.OP_ASSIGN(_) -> parser.fetchNextToken () // eat assignment token
            |_ -> parser.setError("Parser Error: Expecting assignment token.")
        if not parser.parseError then
            forNode.children.Add(parse_Expression parser) // parse initial bound
        if not parser.parseError then
            match parser.curToken with
            |Token.KW_TO(_) |Token.KW_DOWNTO(_) -> 
                let directionNode = {
                    nodeType = NodeTypes.KW
                    value = Some parser.curToken
                    children = new ResizeArray<ASTNode>(0) // no children
                    linkToDeclaration = None
                    attribute = None
                    address = None
                }
                forNode.children.Add(directionNode) // add TO or DOWNTO keyword to forNode
                parser.fetchNextToken ()    // eat TO or DOWNTO token
                forNode.children.Add(parse_Expression parser) // parse final bound
            |_ -> parser.setError("Parser Error: Expecting either tokens TO or DOWNTO in FOR statement")
        if not parser.parseError then
            match parser.curToken with
            |Token.KW_DO(_) -> parser.fetchNextToken () // eat keyword DO
            |_ -> parser.setError("Parse Error: Expecting token DO in FOR statement")
            if not parser.parseError then
                forNode.children.Add(parse_Statement parser)
    //return
    forNode


/// Parse If Statement
and parse_IfStatement parser =
    let ifNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.If_Statement} 
    if not parser.parseError then
        parser.fetchNextToken () // eat IF token
        ifNode.children.Add(parse_Expression parser)
        if not parser.parseError then
            match parser.curToken with
            |Token.KW_THEN(_) -> parser.fetchNextToken () // eat THEN token
            |_ -> parser.setError("Parser Error: Expecting toke THEN in IF statement")
        if not parser.parseError then
            ifNode.children.Add(parse_Statement parser) // parse IF clause
        if not parser.parseError then
            match parser.curToken with
            |Token.KW_ELSE(_) -> 
                parser.fetchNextToken () // eat ELSE token
                ifNode.children.Add(parse_Statement parser) // parse ELSE clause
            |_ -> () // no ELSE clause, do nothing
    //return
    ifNode

/// Parse Case Statement
and parse_CaseStatement parser =
    let caseNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Case_Statement} 
    if not parser.parseError then
        parser.fetchNextToken () // eat CASE token
        caseNode.children.Add(parse_Expression parser)
        if not parser.parseError then
            match parser.curToken with
            |Token.KW_OF(_) -> 
                parser.fetchNextToken () // eat OF token
                caseNode.children.Add(parse_CaseLimb parser)
            |_ -> parser.setError("Parser Error: Expected token OF")
        while (isSemiColon parser.curToken) && (not parser.parseError) do
            parser.fetchNextToken () // eat token semicolon
            match parser.curToken with
            |Token.KW_END(_) -> () // do nothing, exit match to eat END token
            |_ -> caseNode.children.Add(parse_CaseLimb parser)
        match parser.curToken with
        |Token.KW_END(_) -> parser.fetchNextToken () // eat token END
        |_ -> parser.setError("Expecting token END to complete CASE statement")
    //return
    caseNode

/// Parse Case Limb
and parse_CaseLimb parser =
    let caseLimbNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Case_Limb} 
    if not parser.parseError then
        caseLimbNode.children.Add(parse_CaseLabelList parser)
        match parser.curToken with
        |Token.SYM_COLON(_) -> 
            parser.fetchNextToken () // eat colon symbol
            caseLimbNode.children.Add(parse_Statement parser)
        |_-> parser.setError("Parser Error: Expecting Colon after case label list.")
    //return
    caseLimbNode

/// Parse Case Label List
and parse_CaseLabelList parser =
    let labelListNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Case_Label_List} 
    if not parser.parseError then
        labelListNode.children.Add(parse_Constant parser)
    while isComma parser.curToken && not parser.parseError do
        parser.fetchNextToken () // eat comma
        labelListNode.children.Add(parse_Constant parser)
    //return
    labelListNode


// === programs and blocks ===============//

/// Parse Program
let rec parse_Program (parser:ParserDriver) =
    let programNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Program} 
    match parser.curToken with
    |Token.KW_PROGRAM(_) -> programNode.children.Add(parse_ProgramHeading parser)
    |_ -> parser.setError("Parser Error: Expecting token PROGRAM")
    if not parser.parseError then
        programNode.children.Add(parse_Block parser)
    if not parser.parseError then
        match parser.curToken with
        |Token.SYM_DOT(_) -> parser.fetchNextToken () // eat dot token at end of program
        |_ -> parser.setError("Parser Error: Expected '.' at end of program.")
    //return
    programNode

and parse_ProgramHeading parser =
    let phNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Program_Heading} 
    parser.fetchNextToken () // eat PROGRAM token
    match parser.curToken with
    |Token.TK_IDENT(_) -> phNode.children.Add(parse_Identifier parser) // match program name
    |_ -> parser.setError("Parser Error: Expecting an identifier")
    if not parser.parseError then
        match parser.curToken with
        |Token.SYM_LPAREN(_) -> // paren matched -> parse program parameter list
            parser.fetchNextToken () // eat LPAREN token
            phNode.children.Add(parse_IdentifierList parser)
            if not parser.parseError then
                match parser.curToken with
                |Token.SYM_RPAREN(_) -> 
                    parser.fetchNextToken () // eat RPAREN token
                |_ -> parser.setError("Parser Error: Expecting RPAREN token.")
        |_ -> () // do nothing, program has no parameters
    if not parser.parseError then
        match parser.curToken with
            |Token.SYM_SEMICOLON(_) -> parser.fetchNextToken ()
            |_ -> parser.setError("Parser Error: Expecting semicolon.")
    //return
    phNode

/// Parse program identfier list
and parse_IdentifierList parser =
    let ilNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Identifier_List} 
    match parser.curToken with
    |Token.TK_IDENT(_) -> ilNode.children.Add(parse_Identifier parser)
    |_ -> parser.setError("Parser Error: Expecting an identifier.")
    while isComma parser.curToken && not parser.parseError do
        parser.fetchNextToken ()
        match parser.curToken with
        |Token.TK_IDENT(_) -> ilNode.children.Add(parse_Identifier parser)
        |_ -> parser.setError("Parser Error: Expecting an identifier.")
    //return
    ilNode

/// Parse Block
and parse_Block parser =
    let blockNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Block} 
    if not parser.parseError then
        blockNode.children.Add(parse_DeclarationPart parser)
    if not parser.parseError then // parse statement-part
        match parser.curToken with
        |Token.KW_BEGIN(_) ->
            parser.fetchNextToken () // eat BEGIN token
            blockNode.children.Add(parse_StatementSequence parser)
        |_ -> parser.setError("Parser Error: Expecting token BEGIN")
    if not parser.parseError then
        match parser.curToken with
        |Token.KW_END(_) -> parser.fetchNextToken ()
        |_ -> parser.setError("Parser Error: Expecting token END.")
    //return
    blockNode

/// Parse Local Block - Nested functions not yet supported 
and parse_LocalBlock parser =
    let localBlockNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Local_Block} 
    if not parser.parseError then
        localBlockNode.children.Add(parse_LocalDeclarationPart parser)
    if not parser.parseError then // parse statement-part
        match parser.curToken with
        |Token.KW_BEGIN(_) ->
            parser.fetchNextToken () // eat BEGIN token
            localBlockNode.children.Add(parse_StatementSequence parser)
        |_ -> parser.setError("Parser Error: Expecting token BEGIN")
    if not parser.parseError then
        match parser.curToken with
        |Token.KW_END(_) -> parser.fetchNextToken ()
        |_ -> parser.setError("Parser Error: Expecting token END.")
    //return
    localBlockNode

and parse_LocalDeclarationPart parser =
    let localDeclarationsNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Local_Declaration_Part} 
    if not parser.parseError then
        match parser.curToken with
        |Token.KW_CONST(_) ->
            parser.fetchNextToken () // eat CONST token
            localDeclarationsNode.children.Add(parse_ConstantDefinitionPart parser)
        |_ -> ()
    if not parser.parseError then
        match parser.curToken with
        |Token.KW_VAR(_) ->
            parser.fetchNextToken () // eat VAR token
            localDeclarationsNode.children.Add(parse_VariableDeclarationPart parser)
        |_ -> ()
    if not parser.parseError then
        match parser.curToken with
        |Token.KW_FUNCTION(_) |Token.KW_PROCEDURE(_) ->
            parser.setError("Parser Error: Nested functions and procedures not supported")
        |_ -> ()
    //return
    localDeclarationsNode

/// Parse Declarations
and parse_DeclarationPart parser =
    let declarationsNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Declaration_Part} 
    if not parser.parseError then
        match parser.curToken with
        |Token.KW_CONST(_) ->
            parser.fetchNextToken () // eat CONST token
            declarationsNode.children.Add(parse_ConstantDefinitionPart parser)
        |_ -> ()
    if not parser.parseError then
        match parser.curToken with
        |Token.KW_VAR(_) ->
            parser.fetchNextToken () // eat VAR token
            declarationsNode.children.Add(parse_VariableDeclarationPart parser)
        |_ -> ()
    if not parser.parseError then
        match parser.curToken with
        |Token.KW_FUNCTION(_) |Token.KW_PROCEDURE(_) ->
            declarationsNode.children.Add(parse_ProcAndFuncDeclarationPart parser)
        |_ -> ()
    //return
    declarationsNode

/// Parse Constant Definitions
and parse_ConstantDefinitionPart parser =
    let cdNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Constant_Definition_Part} 
    let inline _parseConstDefinition () =
        cdNode.children.Add(parse_ConstantDefinition parser)
        if not parser.parseError then
            match parser.curToken with
            |Token.SYM_SEMICOLON(_) -> parser.fetchNextToken ()
            |_ -> parser.setError("Parser Error: Expecting semicolon after constant definition")
    _parseConstDefinition ()
    while not parser.parseError && isIdentifier parser.curToken do
        _parseConstDefinition ()
    //return
    cdNode

/// Parse Constant Definition
and parse_ConstantDefinition parser =
    let constDefNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Constant_Definition} 
    if isIdentifier parser.curToken then
        constDefNode.children.Add(parse_Identifier parser)
        match parser.curToken with
        |Token.OP_EQUAL(_) -> 
            parser.fetchNextToken () // eat '=' token
            constDefNode.children.Add(parse_Constant parser)
        |_ -> parser.setError("Parser Error: Unexpected token in constant definition")  
    else
        parser.setError("Expecting identifier in LHS of constant definition")
    //return
    constDefNode

/// Parse Variable Declaration Part
and parse_VariableDeclarationPart parser =
    let vdpNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Variable_Declaration_Part} 
    let inline _parseVarDeclaration () =
        vdpNode.children.Add(parse_VariableDeclaration parser)
        if not parser.parseError then
            match parser.curToken with
            |Token.SYM_SEMICOLON(_) -> parser.fetchNextToken ()
            |_ -> parser.setError("Parser Error: Expecting semicolon after variable declaration")
    _parseVarDeclaration ()
    while not parser.parseError && isIdentifier parser.curToken do
        _parseVarDeclaration ()
    //return
    vdpNode

/// Parse Variable Declaration
and parse_VariableDeclaration parser =
    let varDeclNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Variable_Declaration} 
    varDeclNode.children.Add(parse_IdentifierList parser)
    if not parser.parseError then
        match parser.curToken with
        |Token.SYM_COLON(_) -> 
            parser.fetchNextToken () // eat ':' token
            if not parser.parseError then
                varDeclNode.children.Add(parse_Type parser)
        |_ -> parser.setError("Parser Error: Expecting colon Token in variable declaration")
    //return
    varDeclNode

and parse_Type parser  =
    let typeNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Type} 
    match parser.curToken with
    |Token.KW_ARRAY(_) -> 
        parser.fetchNextToken () // eat ARRAY token
        typeNode.children.Add(parse_ArrayType parser)
    |Token.KW_INTEGER(_) |Token.KW_REAL(_) |Token.KW_STRING(_) |Token.KW_BOOLEAN(_) ->
        typeNode.children.Add(parse_RequiredType parser)
    |_ -> typeNode.children.Add(parse_SubrangeType parser)
    //return
    typeNode

/// Parse Rquired Type
and parse_RequiredType parser =
    let reqType = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Required_Type; value = Some parser.curToken} 
    parser.fetchNextToken () // eat type token, e.g.: int, real, bool, etc..
    //return
    reqType

/// Parse Subrange Type
and parse_SubrangeType parser =
    let subrangeTypeNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Subrange_Type} 
    subrangeTypeNode.children.Add(parse_Constant parser) // parse range start
    if not parser.parseError then
        match parser.curToken with
        |Token.SYM_DOTDOT(_) -> 
            parser.fetchNextToken () // eat '..' token
            subrangeTypeNode.children.Add(parse_Constant parser) // parse range end
        |_ -> parser.setError("Parser Error: Expecting '..' token in subrange type")
    //return
    subrangeTypeNode

/// Parse Array type
and parse_ArrayType parser =
    let arrayNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Array_Type} 
    match parser.curToken with
    |Token.SYM_LBRACKET(_) -> parser.fetchNextToken ()
    |_ -> parser.setError("Parser Error: Expecting a '[' token in array declaration")
    if not parser.parseError then
        match parser.curToken with
        |Token.KW_ARRAY(_) -> parser.setError("Parser Error: Multidimensional arrays are not supported.")
        |_ -> 
            arrayNode.children.Add(parse_Expression parser) // parse_size
    if not parser.parseError then
        match parser.curToken with
        |Token.SYM_RBRACKET(_) -> parser.fetchNextToken ()
        |_ -> parser.setError("Parser Error: Expecting ']' token in array declaration.")
    if not parser.parseError then
        match parser.curToken with
        |Token.KW_OF(_) -> parser.fetchNextToken ()
        |_ -> parser.setError("Parser Error: Expecting 'of' token in array declaration")
    if not parser.parseError then
        match parser.curToken with
        |Token.KW_ARRAY(_) -> parser.setError("Parser Error: Multidimensional arrays are not supported.")
        |Token.KW_INTEGER(_) |Token.KW_REAL(_) |Token.KW_STRING(_) |Token.KW_BOOLEAN(_) ->
            arrayNode.children.Add(parse_RequiredType parser) // parse element type
        |_ -> parser.setError "Parser Error: Unexpected token while parsing array type."
    // return
    arrayNode

/// Parse Procedure and Function Declaration Part
and parse_ProcAndFuncDeclarationPart parser =
    let pfdNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Procedure_And_Function_Declaration_Part} 
    let isFuncOrProc token =
        match parser.curToken with
        |Token.KW_PROCEDURE(_) |Token.KW_FUNCTION(_) -> true
        |_ -> false

    while not parser.parseError && (isFuncOrProc parser.curToken) do
        match parser.curToken with
        |Token.KW_PROCEDURE(_) -> 
            pfdNode.children.Add(parse_ProcedureDeclaration parser)
            if isSemiColon parser.curToken then
                parser.fetchNextToken () // eat ';' token
            else
                parser.setError("Parser Error: Expecting ';' after procedure declaration.")
        |Token.KW_FUNCTION(_) -> 
            pfdNode.children.Add(parse_FunctionDeclaration parser)
            if isSemiColon parser.curToken then
                parser.fetchNextToken () // eat ';' token
            else
                parser.setError("Parser Error: Expecting ';' after function declaration.")
        |_ -> ()
    // return
    pfdNode

/// Parse Procedure Declaration
and parse_ProcedureDeclaration parser =
    let procDeclNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Procedure_Declaration} 
    procDeclNode.children.Add(parse_ProcedureHeading parser) 
    if not parser.parseError then
        match parser.curToken with
        |Token.SYM_SEMICOLON(_) ->
            parser.fetchNextToken () // eat ';' after proc heading
            procDeclNode.children.Add(parse_LocalBlock parser) // parse procedure body
        |_ -> parser.setError("Parser Error: Expecting ';' token after procedure heading.")
    //return
    procDeclNode

/// Parse Procedure Heading
and parse_ProcedureHeading parser =
    let procHeadingNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Procedure_Heading} 
    parser.fetchNextToken () // eat PROCEDURE token
    match parser.curToken with
    |Token.TK_IDENT(_) -> procHeadingNode.children.Add(parse_Identifier parser) // parse proc name
    |_ -> parser.setError("Parser Error: Expecting identifier in procedure declaration.")
    if not parser.parseError then
        match parser.curToken with
        |Token.SYM_LPAREN(_) ->
            parser.fetchNextToken ()
            procHeadingNode.children.Add(parse_FormalParameterList parser)
            if not parser.parseError then
                match parser.curToken with
                |Token.SYM_RPAREN(_) -> parser.fetchNextToken ()
                |_ -> parser.setError("Parser Error: Expecting ')' token in procedure parameter list.")
        |_ -> () // no procedure parameters: do nothing
    //return
    procHeadingNode

/// Parse Formal Parameter List
and parse_FormalParameterList parser =
    let fplNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Formal_Parameter_List} 
    if not parser.parseError then
        fplNode.children.Add(parse_FormalParameterSection parser)
    while not parser.parseError && (isSemiColon parser.curToken) do
        parser.fetchNextToken () // eat ';' token
        fplNode.children.Add(parse_FormalParameterSection parser)
    //return
    fplNode

/// Parse Formal Parameter Section
and parse_FormalParameterSection parser =
    let fpsNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Formal_Parameter_Section} 
    match parser.curToken with
    |Token.TK_IDENT(_) -> fpsNode.children.Add(parse_ValueParameterSection parser)
    |Token.KW_VAR(_) -> parser.setError("Parser Error: Variable parameters are not supported")
    |Token.KW_FUNCTION(_) | Token.KW_PROCEDURE(_) ->
        parser.setError("Parser Error: Function and procedure parameters not supported.")
    |_ -> parser.setError("Parser Error: Unexpected token in procedure formal parameter list")
    //return
    fpsNode


/// Parse Value Parameter Section
and parse_ValueParameterSection parser =
    let vpsNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Value_Parameter_Section} 
    if not parser.parseError then
        vpsNode.children.Add(parse_IdentifierList parser)
    if not parser.parseError then
        match parser.curToken with
        |Token.SYM_COLON(_) -> 
            parser.fetchNextToken ()
            vpsNode.children.Add(parse_Type parser) // parse type of parameter list
        |_ -> parser.setError("Parser Error: Expecting ':' in procedure parameter declaration")
    //return
    vpsNode

/// Parse Function Declaration
and parse_FunctionDeclaration parser =
    let funcDeclNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Function_Declaration} 
    funcDeclNode.children.Add(parse_FunctionHeading parser)
    if not parser.parseError then
        match parser.curToken with
        |Token.SYM_SEMICOLON(_) ->
            parser.fetchNextToken () // eat ';' token
            funcDeclNode.children.Add(parse_LocalBlock parser) // parse function body
        |_ -> parser.setError("Parser Error: Expecting ';' token after function heading.")
    //return
    funcDeclNode

/// Parse Function  Heading
and parse_FunctionHeading parser =
    let funcHeadingNode = {defaultNode with children = ResizeArray<ASTNode>(); nodeType = NodeTypes.Function_Heading} 
    parser.fetchNextToken () // eat FUNCTION token
    match parser.curToken with
    |Token.TK_IDENT(_) -> funcHeadingNode.children.Add(parse_Identifier parser) // parse func name
    |_ -> parser.setError("Parser Error: Expecting identifier in function declaration.")
    match parser.curToken with
    |Token.SYM_LPAREN(_) ->
        parser.fetchNextToken ()
        funcHeadingNode.children.Add(parse_FormalParameterList parser)
        if not parser.parseError then
            match parser.curToken with
            |Token.SYM_RPAREN(_) -> parser.fetchNextToken ()
            |_ -> parser.setError("Parser Error: Expecting ')' token in function parameter list.")
    |_ -> () // no func parameters: do nothing
    if not parser.parseError then
        match parser.curToken with
        |Token.SYM_COLON(_) -> 
            parser.fetchNextToken ()
            funcHeadingNode.children.Add(parse_Type parser)
        |_ -> parser.setError("Parser Error: Expecting ':' token in function declaration") 
    //return
    funcHeadingNode

/// ======================== main parser function ========================
let parse (tokens : ResizeArray<Token>) =
    let parser = {
        tokenList = tokens
        pos = -1
        curToken = Token.TK_EMPTYORDEFAULT
        parseError = false
        errorMessage = None
    }
    parser.fetchNextToken ()    // fetch first token to begin parsing
    let ast = parse_Program parser
    // if all tokens not consumed -> parser error
    if not parser.parseError && not (parser.curToken = Token.TK_EOF) then
        parser.setError ("Unexpected Token:" + parser.curToken.ToString())
    (ast, parser.errorMessage)
