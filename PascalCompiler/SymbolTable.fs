﻿module SymbolTable

open System
open System.Collections.Generic
open System.Linq

open PascalTokens
open NodeTypes
open Attributes
open ErrorReporting

/// Type alias for Dictionary - represent a local symbol table
type Table = Dictionary<string, Attribute * ASTNode>

/// Global Symbol Table data structure
type GlobalTable = {
    /// Stack of symbol tables
    tableStack : Table Stack
    /// Level of deepest nested scope
    mutable maxScope : int
}
with
    /// Method to load standard environment in scope 0 of symbol table
    member this.loadStandardEnvironment (reporter : ErrorReporter) = 
        this.openScope ()
        let writeln = Token.TK_IDENT ("writeln",0)
        let node = {
            nodeType = NodeTypes.System_Procedure_Identifier
            value = Some writeln
            children = ResizeArray<ASTNode>()
            attribute = None
            linkToDeclaration = None
            address = None
        }
        let attribute = SysProc (ResizeArray<Type>())
        this.addSymbol node.value.Value (attribute, node) reporter

    /// Open a new scope: push a new symbol table on the stack
    member this.openScope () = 
        this.tableStack.Push(Table())
        this.maxScope <- this.maxScope + 1

    /// Close top scope: pop symbol table from the stack
    member this.closeScope () =
        if this.maxScope >= 0 then
            this.tableStack.Pop() |> ignore
            this.maxScope <- this.maxScope - 1

    /// Add a new identifier to the symbol table
    member this.addSymbol token (attribute, nodePtr) (errorReporter : ErrorReporter) =
        let identifier =
            match token with
            |Token.TK_IDENT(id, line) -> id
            |_ -> errorReporter.setError ErrorType.SemanticError "Invalid token type passed to symbol table"; ""
        if not errorReporter.errorState then
            if this.tableStack.ElementAt(0).ContainsKey(identifier) then
                errorReporter.setError ErrorType.SemanticError ("Duplicate declaration of identifier: " + identifier)
            else
                this.tableStack.ElementAt(0).Add(identifier, (attribute, nodePtr))

    /// Lookup identifier in symbol table
    member this.lookup token (errorReporter : ErrorReporter) =
        let identifier = 
            match token with
            |Token.TK_IDENT(str, _) -> str
            |_ -> errorReporter.setError ErrorType.SemanticError "Invalid token type passed to symbol table"; ""
        let rec find id scope =
            match (this.maxScope - scope) with
            |(-1) -> 
                errorReporter.setError ErrorType.SemanticError ("The identifier: " + id + " is undefined.")
                None
            |_ -> 
                if this.tableStack.ElementAt(scope).ContainsKey(id) then
                    Some (this.tableStack.ElementAt(scope).[id])
                else find id (scope + 1)
        find identifier 0 
end