﻿// signature file for Parser

[<RequireQualifiedAccess>]
module Parser

val  parse :  ResizeArray<PascalTokens.Token> -> NodeTypes.ASTNode * Option<string>
