﻿module Attributes


type Type =
    |Int
    |Real
    |Bool
    |String
    |Nil
    

type Attribute =
    |Variable of Type
    |Constant of Type
    |Expression of Type
    |Array of Type
    |Procedure of Type ResizeArray
    |Function of Type ResizeArray * Type
    |SysProc of Type ResizeArray
    |SysFunc of Type ResizeArray * Type


