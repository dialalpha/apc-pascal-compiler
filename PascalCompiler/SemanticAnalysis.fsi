﻿// Signature file for Semantic Analysis

[<RequireQualifiedAccess>]
module SemanticAnalysis

val semanticCheck : NodeTypes.ASTNode -> NodeTypes.ASTNode option
