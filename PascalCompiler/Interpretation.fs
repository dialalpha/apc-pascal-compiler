﻿module Interpretation

open VirtualMachine
open NodeTypes
open PascalTokens

let generateAddress (machine : Interpreter) =
    match machine.inLocalFrame with
    |true -> Some (1, machine.ST - machine.LB)
    |false -> Some (0, machine.ST - machine.LB)

let getAddress (node : ASTNode) =
    match node.linkToDeclaration with
    |Some n -> n.address.Value
    |_ -> failwith "Runtime Error: Attempted to access unknown node address"

let rec execute_Program (machine : Interpreter) (program : ASTNode) =
    let programHeading = program.children.[0]
    let block = program.children.[1]
    execute_ProgramHeading machine programHeading
    execute_Block machine block

and execute_ProgramHeading machine programHeading = () // TODO

and execute_Block machine block = 
    let declarationPart = block.children.[0]
    let statementSequence = block.children.[1] // statement part
    execute_DeclarationPart machine declarationPart 
    execute_StatementSequence machine statementSequence // execute statement part

and execute_DeclarationPart machine declarationPart =
    for child in declarationPart.children do
        match child.nodeType with
        |NodeTypes.Constant_Definition_Part -> 
            execute_ConstantDefinitionPart machine child
        |NodeTypes.Variable_Declaration_Part ->
            execute_VariableDeclarationPart machine child
        |NodeTypes.Procedure_And_Function_Declaration_Part ->
            execute_ProcedureAndFunctionDeclarationPart machine child
        |_ -> failwith "Compiler: Fatal Error - Unmatched node in <Declaration Part>"

and execute_ConstantDefinitionPart machine constantDefinitionPart =
    for child in constantDefinitionPart.children do
        execute_ConstantDefinition machine child

and execute_ConstantDefinition machine constantDefinition =
    constantDefinition.address <- generateAddress machine
    let data = get_Constant machine constantDefinition.children.[1] // extract constant
    machine.pushImmediate data

and get_Constant machine constant =
    let dataIndex =
        match constant.children.[0].nodeType with
        |NodeTypes.Sign -> 1
        |_ -> 0
    let dataNode = constant.children.[dataIndex]
    let data =
        match dataNode.value.Value with
        |Token.KW_TRUE(_) -> Bool true
        |Token.KW_FALSE(_) -> Bool false
        |Token.KW_NIL(_) -> Nil
        |Token.TK_INTLIT(i, _) -> Int i
        |Token.TK_STRLIT(str, _) -> Data.String str
        |Token.TK_REALLIT(r, _) -> Real r
        |Token.TK_IDENT(ident, _) ->
            let address = getAddress dataNode 
            machine.readPrimitive address
        |_ -> failwith "Runtime Error: Unable to determine constant value."
    if dataIndex = 1 then // sign present before constant value
        eval_unaryOp constant.children.[0] data
    else data

and execute_VariableDeclarationPart machine varDeclPart =
    for varDecl in varDeclPart.children do
        let identifierList = varDecl.children.[0]
        let typeNode = varDecl.children.[1]     // parse_Type
        let actualType = typeNode.children.[0]  // parse_ArrayType
        match actualType.nodeType with
        |NodeTypes.Array_Type -> 
            let sizeNode = actualType.children.[0]
            let arraySize = evaluate_Expression machine sizeNode
            allocate_ArrayIdentifierList machine arraySize identifierList
        |_ -> allocate_SimpleIdentifierList machine identifierList // simpleType

/// allocate simple types: int, real, bool, str
and allocate_SimpleIdentifierList machine identifierList =
    for identifier in identifierList.children do
        identifier.address <- generateAddress machine
        machine.allocateVar ()

/// allocate array type: create empty array of correct size and push to storage
and allocate_ArrayIdentifierList machine arraySize identifierList =
    let size = 
        match arraySize with
        |Int n -> n
        |_ -> failwith "Runtime Error: Array size must evaluate to <int>"
    for identifier in identifierList.children do
        let (arr : Data array) = Array.zeroCreate size
        identifier.address <- generateAddress machine
        machine.pushImmediate (Array arr)

and execute_ProcedureAndFunctionDeclarationPart machine pfdPart =
    for child in pfdPart.children do
        match child.nodeType with
        |NodeTypes.Procedure_Declaration -> execute_ProcedureDeclaration machine child
        |NodeTypes.Function_Declaration -> failwith "Runtime Error: Functions are not supported."
        |_ -> failwith "Runtime Error: Unknown node type in <proc-or-func-decl> part"

and execute_ProcedureDeclaration machine procedure =
    machine.pushStackFrame ()
    let procHeading = procedure.children.[0]
    if procHeading.children.Count > 1 then
        let formalParamList = procHeading.children.[1]
        for formalParamSection in formalParamList.children do
            let valueParamSection = formalParamSection.children.[0]
            let identifierList = valueParamSection.children.[0]
            allocate_SimpleIdentifierList machine identifierList
    machine.popStackFrame ()

and execute_StatementSequence machine statementSequence = 
    for statement in statementSequence.children do
        execute_Statement machine statement

and evaluate_Expression (machine : Interpreter) (expression : ASTNode) =
    let se1 =  expression.children.[0] // simple_expression
    let result1 = evaluate_SimpleExpression machine se1
    if expression.children.Count > 1 then // relop present
        let se2 = expression.children.[2] // simple_expression
        let relationalOperator = expression.children.[1]
        let result2 = evaluate_SimpleExpression machine se2
        eval_binaryOp relationalOperator result1 result2
    else result1

and evaluate_SimpleExpression machine se =
    let signPresent = // does expression begin with a '+/-'?
        match se.children.[0].nodeType with
        |NodeTypes.Sign -> true
        |_ -> false
    let ops, terms =
        if signPresent then
            se.children
            |> Seq.skip 1
            |> Seq.toArray
            |> Array.partition (fun e -> e.nodeType = NodeTypes.Addition_Operator)
        else
            se.children
            |> Seq.toArray
            |> Array.partition (fun e -> e.nodeType = NodeTypes.Addition_Operator)
    let termData = terms |> Array.map (evaluate_Term machine) // evaluation results
    if signPresent then 
        let sign = se.children.[0]
        termData.[0] <- eval_unaryOp sign termData.[0]
    if terms.Length = 1 then
        termData.[0]
    else // add ops present
        Array.fold2 (fun arg1 op arg2 -> eval_binaryOp op arg1 arg2) 
            termData.[0] ops termData.[1 .. (termData.Length - 1)]

and evaluate_Term machine term =
    let ops, factors =
        term.children
        |> Seq.toArray
        |> Array.partition (fun t -> t.nodeType = NodeTypes.Multiplication_Operator)
    let factorData = factors |> Array.map (evaluate_Factor machine)
    if factors.Length = 1 then
        factorData.[0]
    else // multops found
        Array.fold2 (fun arg1 op arg2 -> eval_binaryOp op arg1 arg2)
            factorData.[0] ops factorData.[1 .. (factorData.Length - 1)]

and evaluate_Factor machine factor =
    let actualFactor = factor.children.[0]
    match actualFactor.nodeType with
    |NodeTypes.KW ->
        match actualFactor.value.Value with
        |Token.OP_NOT(_) ->
            let innerFactor = factor.children.[1]
            eval_unaryOp actualFactor (evaluate_Factor machine innerFactor)
        |Token.KW_TRUE(_) -> Bool true
        |Token.KW_FALSE(_) -> Bool false
        |_ -> Nil
    |NodeTypes.Number ->
        match actualFactor.value.Value with
        |Token.TK_INTLIT(value, _) -> Int value
        |Token.TK_REALLIT(value, _) -> Real value
        |_ -> failwith "Runtime Error: Failed in evaluating number node in <factor>" // should never match
    |NodeTypes.String ->
        match actualFactor.value.Value with
        |Token.TK_STRLIT(str, _) -> Data.String str
        |_ -> failwith "Runtime Error: Failed in evaluating string node in <factor>" // should never match
    |NodeTypes.Expression -> evaluate_Expression machine actualFactor
    |NodeTypes.Identifier ->
        let address = getAddress actualFactor 
        machine.readPrimitive address
    |NodeTypes.Array_Variable ->
        let arrayName = actualFactor.children.[0]
        let indexExpression = actualFactor.children.[1]
        let baseAddress = arrayName.linkToDeclaration.Value.address.Value
        let index =
            match (evaluate_Expression machine indexExpression) with
            |Int i -> i
            |_ -> failwith "Runtime Error: Array index must evaluate to <int>."
        machine.readArray baseAddress index
    |_ -> failwith "Runtime Error: Failed evaluating <factor>. No matching nodeType"

and execute_Statement machine statement =
    if statement.children.Count > 0 then
        let actualStatement = statement.children.[0]
        match actualStatement.nodeType with
        |NodeTypes.Var_Assignment_Statement -> 
            execute_VarAssignmentStatement machine actualStatement
        |NodeTypes.Array_Assignment_Statement -> 
            execute_ArrayAssignmentStatement machine actualStatement
        |NodeTypes.Procedure_Statement ->
            execute_ProcedureStatement machine actualStatement
        |NodeTypes.Compound_Statement ->
            execute_CompoundStatement machine actualStatement
        |NodeTypes.While_Statement ->
            execute_WhileStatement machine actualStatement
        |NodeTypes.For_Statement ->
            execute_ForStatement machine actualStatement
        |NodeTypes.Repeat_Statement ->
            execute_RepeatStatement machine actualStatement
        |NodeTypes.If_Statement ->
            execute_IfStatement machine actualStatement
        |NodeTypes.Case_Statement ->
            execute_CaseStatement machine actualStatement
        |_ -> () // empty statement

and execute_VarAssignmentStatement machine statement =
    let identifier = statement.children.[0] // lhs
    let expression = statement.children.[1] // rhs
    let storageAddress = getAddress identifier
    let data = evaluate_Expression machine expression
    // cast result between int <-> real if necessary
    match identifier.attribute.Value, data with
    |Attributes.Variable (Attributes.Int), Real r ->
        let convertedData = Int (int r)     // real -> int
        machine.assignPrimitive storageAddress convertedData
    |Attributes.Variable (Attributes.Real), Int i ->
        let convertedData = Real (single i) // int -> real
        machine.assignPrimitive storageAddress convertedData
    |_ -> machine.assignPrimitive storageAddress data

and execute_ArrayAssignmentStatement machine statement =
    let lhs = statement.children.[0]        // array variable
    let rhsExpression = statement.children.[1] // rhs expression
    let identifier = lhs.children.[0]       // array identifier
    let indexExpression = lhs.children.[1]  // array index to assign to
    let data = evaluate_Expression machine rhsExpression
    let arrayBaseAddress = getAddress identifier
    let arrayIndex = 
        match (evaluate_Expression machine indexExpression) with
        |Int i -> i
        |_ -> failwith "Runtime Error: Array index did not evaluate to <int>"
    // cast result before assignment if necessary
    match identifier.attribute.Value, data with
    |Attributes.Array (Attributes.Int), Real r ->
        let convertedData = Int (int r) // real -> int
        machine.assignArray arrayBaseAddress arrayIndex convertedData
    |Attributes.Array (Attributes.Real), Int i ->
        let convertedData = Real (single i)
        machine.assignArray arrayBaseAddress arrayIndex convertedData
    |_ -> machine.assignArray arrayBaseAddress arrayIndex data

and execute_CompoundStatement machine statement =
    execute_StatementSequence machine statement.children.[0] // jus run statement sequence

and execute_WhileStatement machine statement =
    let condition = statement.children.[0]
    let body = statement.children.[1]
    while (evaluate_Expression machine condition) = (Bool true) do
        execute_Statement machine body

and execute_IfStatement machine statement =
    let condition = statement.children.[0]
    let ifBody = statement.children.[1]
    if (evaluate_Expression machine condition) = (Bool true) then
        execute_Statement machine ifBody
    elif statement.children.Count > 2 then // else clause present -> execute
        execute_Statement machine statement.children.[2]

and execute_RepeatStatement machine statement = 
    let body = statement.children.[0]   //statement sequence
    let condition = statement.children.[1]
    execute_StatementSequence machine body
    while (evaluate_Expression machine condition) = (Bool false) do
        execute_StatementSequence machine body

and execute_ForStatement machine statement =
    let identifierAddress = getAddress statement.children.[0]
    let initBound = evaluate_Expression machine statement.children.[1]
    let direction = statement.children.[2].value.Value // to | downto
    let finalBound = evaluate_Expression machine statement.children.[3]
    let body = statement.children.[4]
    let isBoundSatisfied init final current =
        match init, final, current with
        |Int i, Int f, Int c -> (c >= i && c <= f)
        |_ -> failwith "Runtime Error: Failed matching array bounds"
    machine.assignPrimitive identifierAddress initBound
    while (isBoundSatisfied initBound finalBound (machine.readPrimitive identifierAddress) ) do
        execute_Statement machine body
        let current = machine.readPrimitive identifierAddress
        match direction with
        |Token.KW_TO(_) ->
            let newCurrent =
                match current with
                |Int i -> Int (i + 1)
                |_ -> failwith "Runtime Error: Failed updating <for> bound."
            machine.assignPrimitive identifierAddress newCurrent
        |_ ->
            let newCurrent =
                match current with
                |Int i -> Int (i - 1)
                |_ -> failwith "Runtime Error: Failed updating <for> bound."
            machine.assignPrimitive identifierAddress newCurrent

and execute_CaseStatement machine statement =
    let expression = statement.children.[0] // match this
    let cases = statement.children |> Seq.skip 1 // case limbs
    let expEval = evaluate_Expression machine expression
    try
        let matchedStatement = 
            seq { for case in cases do
                    let labels = case.children.[0]
                    let statement = case.children.[1]
                    if Seq.exists (fun l -> expEval = get_Constant machine l) labels.children then
                        yield statement}
            |> Seq.head
        execute_Statement machine matchedStatement
    with
    |_ -> failwith "Runtime error: Unresolved <case> expression."

and execute_ProcedureStatement machine statement =
    let identifier = statement.children.[0]
    let actualParamsList = statement.children |> Seq.skip 1
    match identifier.attribute.Value with
    |Attributes.SysProc _ ->
        for arg in actualParamsList do
            let argValue = evaluate_Expression machine arg
            match argValue with
            |Int i -> printf "%i" i
            |Real r -> printf "%f" r
            |Bool b -> printf "%b" b
            |Data.String s -> printf "%s" s
            |Array a -> printf "%A" a
            |Nil -> ()
        printfn ""
    |_ -> // is user-defined procedure
        machine.pushStackFrame ()
        for arg in actualParamsList do
            machine.pushImmediate (evaluate_Expression machine arg)
        let procBody = identifier.linkToDeclaration.Value.children.[1]
        execute_Block machine procBody
        machine.popStackFrame ()

// ========= Run Program ====
let runProgram programNode =
    let machine = {
        dataStore = Array.zeroCreate 1024
        FP = System.Collections.Generic.Stack<int>()
        SB = 0
        LB = 0
        ST = 0
        inLocalFrame = false
    }
    execute_Program machine programNode