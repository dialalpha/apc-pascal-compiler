﻿/// ALL DA TESTS!!!!

module Program

open System
open PascalTokens

let prettyPrint (node : NodeTypes.ASTNode) =
    let rec print (node : NodeTypes.ASTNode) indentLevel =
        if indentLevel > 0 then
            for i in 1 .. indentLevel do
                printf "  "
        node.printNode ()
        printfn ""
        for child in node.children do
            print child (indentLevel + 1)
    print node 0

[<EntryPoint>]
let main _ = 
    
    let tokens = Scanner.scan @"c:\users\diala_000\desktop\pascal\test.pas"
    let (AST, err) = Parser.parse tokens
    if err.IsSome then
        printfn "\n========Parser Error========="
        printfn "%A" err.Value
    printfn "\nAbstract Syntax Tree: \n"
    let decoratedAST =
        if err.IsNone then SemanticAnalysis.semanticCheck AST
        else None
    if decoratedAST.IsSome then
        printfn "\nSemantic check success. \nNow Running program:\n"
        //prettyPrint decoratedAST.Value
        Interpretation.runProgram decoratedAST.Value
    Console.ReadKey(true) |> ignore

    
    0 // return an integer exit code
    
