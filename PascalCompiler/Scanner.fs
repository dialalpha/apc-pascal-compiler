﻿(*
    ==== The big picture =====
    
    This file/module implements the scanner/lexer portion of the compiler
    Use of components requires qualified access
    
    We declare and use a LexerDriver type (of record) which holds info about
    the current file being scanned and also generates the list of tokens.
    The lexer holds a char buffer of the file, and maintains the 
    position of the scanner pointer, line number, column number, 
    current token being scanned, last token scanned, etc...

    LexerDriver uses four instance members:
        * LexerDriver.nextChar(), which returns the next char in the buffer
            and _advances_ the scanner pointer.
        * LexerDriver.curChar(), which simply return the char at the current position
        * LexerDriver.peek(), which returns the next char without
            advancing the scanner pointer
        * LexerDriver.backtrack, which takes an int arg and regresses
            the scaner pointer by that much

    The second major component of this module is the scan function.
    This is the only public component of the module.
    Here we instantiate a new LexerDriver type, read the input file
    into the LexerDriver's buffer, and finally loop through all the 
    characters therein returning new tokens as they're matched.
    This function returns a List<Token>. The Token type is a discriminated
    union defined in the PascalTokens module (PascalTokens.fs).

    The remainder of the stand-alone functions in this module
    mostly serve to match different types of tokens such as keywords,
    identifiers, literals, symbols, etc... They are all called from
    the scan function described above, and are not accessible outside
    of the module.
*)

module Scanner

open System
open System.IO
open System.Text
open System.Collections.Generic
open PascalTokens


/// the scanner structure - organized as a record
type LexerDriver = {
    chars : char[]                              // source character array
    mutable pos : int                           // current position in char array: init to -1
    mutable curToken : StringBuilder            // current token being scanned: init to ""
    mutable lineNo : int                        // current line: init to 1
    mutable colNo : int                         // current column: init to 1
    mutable lastTok : Token}                    // latest token matched by lexer: init to TK_EmptyOrDefault
    with
        member this.nextChar() =                  // advance scanner pointer and ret char at that pos
            if this.pos < this.chars.Length - 1 then
                this.pos <- (this.pos + 1)
                this.colNo <- (this.colNo + 1)
                this.chars.[this.pos]
            else
                Char.MinValue                  // return empty string at EOF

        member this.peek() =                      // check next char w/out advancing scanner ptr
            if this.pos < this.chars.Length - 1 then
                this.chars.[(this.pos + 1)]
            else
                Char.MinValue                  // return empty string at EOF

        member this.curChar() =                   // ret char at current pos
            if (this.pos < this.chars.Length) && (this.pos >= 0) then
                this.chars.[this.pos]
            else Char.MinValue                  // return empty string at EOF

        member this.backtrack i = 
            this.pos <- this.pos - i
            this.colNo <- this.colNo - i
            if this.pos < -1 then this.pos <- -1
    end

/// matches symbolic IDs to approriate operator or symbol
let matchSymbolic lineNo token =
    match token with 
    |"/" -> OP_REALDIV(lineNo)
    |"+" -> OP_ADD(lineNo)
    |"-" -> OP_SUB(lineNo)
    |"*" -> OP_MULT(lineNo)
    |":=" -> OP_ASSIGN(lineNo)
    |"=" -> OP_EQUAL(lineNo)
    |"<>" -> OP_NOTEQUAL(lineNo)
    |"<" -> OP_LESS(lineNo)
    |">" -> OP_GREAT(lineNo)
    |"<=" -> OP_LESSEQ(lineNo)
    |">=" -> OP_GREATEQ(lineNo)
    |"(" -> SYM_LPAREN(lineNo)
    |")" -> SYM_RPAREN(lineNo)
    |"[" -> SYM_LBRACKET(lineNo)
    |"]" -> SYM_RBRACKET(lineNo)
    |"^" -> SYM_CARET(lineNo)
    |"@" -> SYM_AT(lineNo)
    |"," -> SYM_COMMA(lineNo)
    |"." -> SYM_DOT(lineNo)
    |":" -> SYM_COLON(lineNo)
    |";" -> SYM_SEMICOLON(lineNo)
    |".." -> SYM_DOTDOT(lineNo)
    |err -> TK_ERROR(err, lineNo)

/// matches identifiers and keywords that start with alphabetic char
let matchAlphaNumIdent lineNo token = 
    match token with 
    |"array" -> KW_ARRAY(lineNo)
    |"begin" -> KW_BEGIN(lineNo)
    |"boolean" -> KW_BOOLEAN(lineNo)
    |"case" -> KW_CASE(lineNo)
    |"character" -> KW_CHARACTER(lineNo)
    |"const" -> KW_CONST(lineNo)
    |"do" -> KW_DO(lineNo)
    |"downto" -> KW_DOWNTO(lineNo)
    |"else" -> KW_ELSE(lineNo)
    |"end" -> KW_END(lineNo)
    |"file" -> KW_FILE(lineNo)
    |"for" -> KW_FOR(lineNo)
    |"function" -> KW_FUNCTION(lineNo)
    |"goto" -> KW_GOTO(lineNo)
    |"if" -> KW_IF(lineNo)
    |"label" -> KW_LABEL(lineNo)
    |"nil" -> KW_NIL(lineNo)
    |"of" -> KW_OF(lineNo)
    |"packed" -> KW_PACKED(lineNo)
    |"procedure" -> KW_PROCEDURE(lineNo)
    |"program" -> KW_PROGRAM(lineNo)
    |"record" -> KW_RECORD(lineNo)
    |"repeat" -> KW_REPEAT(lineNo)
    |"set" -> KW_SET(lineNo)
    |"string" -> KW_STRING(lineNo)
    |"to" -> KW_TO(lineNo)
    |"then" -> KW_THEN(lineNo)
    |"until" -> KW_UNTIL(lineNo)
    |"var" -> KW_VAR(lineNo)
    |"while" -> KW_WHILE(lineNo)
    |"with" -> KW_WITH(lineNo)
    |"integer" -> KW_INTEGER(lineNo)
    |"real" -> KW_REAL(lineNo)
    |"false" -> KW_FALSE(lineNo)
    |"true" -> KW_TRUE(lineNo)
    |"and" -> OP_AND(lineNo)
    |"or" -> OP_OR(lineNo)
    |"not" -> OP_NOT(lineNo)
    |"xor" -> OP_XOR(lineNo)
    |"in" -> OP_IN(lineNo)
    |"div" -> OP_INTDIV(lineNo)
    |"mod" -> OP_INTMOD(lineNo)
    |"shl" -> OP_SHL(lineNo)
    |"shr" -> OP_SHR(lineNo)
    | id -> TK_IDENT(id, lineNo)

/// read content of file into a char buffer
let readFileIntoCharArray (file : string) =
    try
        use sr = new StreamReader(file)
        sr.ReadToEnd().ToLower().ToCharArray()
    with
        |ex -> 
        failwith ("Problem reading file: " + ex.Message)

/// active pattern to classify how to match new token
let (|IsLetter|IsDigit|IsQuote|IsWhiteSpace|IsComment|MaybeComment|IsSymbol|) c =
    if Char.IsLetter(c) then IsLetter
    elif Char.IsDigit(c) then IsDigit
    elif c = '\'' then IsQuote
    elif Char.IsWhiteSpace(c) then IsWhiteSpace
    elif c = '{' then IsComment
    elif c = '(' then MaybeComment
    else IsSymbol

/// Eat comments
let eatComments (lexer: LexerDriver) (tokList : List<Token>) =
    lexer.curToken.Clear() |> ignore
    if lexer.curChar() = '(' then
        if lexer.peek() = '*' then
            lexer.nextChar() |> ignore
    let mutable withinComment = true
    while withinComment do
        // check for new line
        if lexer.nextChar() = '\013' && lexer.peek() = '\010' then
            lexer.nextChar() |> ignore
            lexer.colNo <- 1
            lexer.lineNo <- lexer.lineNo + 1
        elif lexer.curChar() = '*' && lexer.peek() = ')' then
            withinComment <- false
            lexer.nextChar() |> ignore
        elif lexer.curChar() = '}' then
            withinComment <- false
        elif lexer.peek() = Char.MinValue then
            withinComment <- false
            lexer.lastTok <- TK_ERROR("Unclosed Comment", lexer.lineNo) 
            tokList.Add(lexer.lastTok)

/// Scan string or char literal
let scanString (lexer : LexerDriver) =
    lexer.curToken.Clear() |> ignore
    let mutable openString = true
    let mutable validString = true
    let mutable EOL = false
    while openString && (not EOL) do
        if lexer.nextChar() = Char.MinValue then // advances scanp, check for EOF
            EOL <- true
            validString <- false
        elif lexer.curChar() = '\013' && lexer.peek() = '\010' then //check for line break
            lexer.nextChar() |> ignore
            lexer.lineNo <- (lexer.lineNo + 1)
            lexer.colNo <- 0
            validString <- false
        elif lexer.curChar() = '\'' then
            openString <- false
        else 
            lexer.curToken.Append(lexer.curChar()) |> ignore
    if validString then
        TK_STRLIT(lexer.curToken.ToString(), lexer.lineNo)
    else
        TK_ERROR("Invalid String literal", lexer.lineNo)
            

/// scan token that begins with a symbol
let scanSymbolic (lexer : LexerDriver) =
    lexer.curToken.Clear() |> ignore
    lexer.curToken.Append(lexer.curChar()) |> ignore
    match lexer.curChar() with
    |':' ->
        if lexer.peek() = '=' then
            lexer.curToken.Append(lexer.nextChar()) |> ignore
    |'<' ->
        if lexer.peek() = '>' || lexer.peek() = '=' then
            lexer.curToken.Append(lexer.nextChar()) |> ignore
    |'>' ->
        if lexer.peek() = '=' then
            lexer.curToken.Append(lexer.nextChar()) |> ignore
    |'.' ->
        if lexer.peek() = '.' then
            lexer.curToken.Append(lexer.nextChar()) |> ignore
    |_ -> ()  //catch all, including TK_ERROR

    matchSymbolic lexer.lineNo (lexer.curToken.ToString())

/// scans for an alphanumeric identifier: user-defined, or reserved keyword
let scanAlphaNumericID (lexer : LexerDriver) =
    lexer.curToken.Clear() |> ignore
    lexer.curToken.Append(lexer.curChar()) |> ignore      
    while Char.IsLetterOrDigit(lexer.peek()) do
        lexer.curToken.Append(lexer.nextChar()) |> ignore
    lexer.curToken.ToString() |> matchAlphaNumIdent lexer.lineNo

/// scan numeric lit
let scanNumeric (lexer : LexerDriver) =
    lexer.curToken.Clear() |>ignore
    lexer.curToken.Append(lexer.curChar()) |> ignore
    while Char.IsDigit(lexer.peek()) do
        lexer.curToken.Append(lexer.nextChar()) |> ignore
    if lexer.peek() = '.' then
        lexer.curToken.Append(lexer.nextChar()) |>ignore
        if lexer.peek() = '.' then    // potential float
            lexer.curToken.Remove(- 1 + lexer.curToken.Length, 1) |> ignore // '..' symbol found
            lexer.backtrack 1
        else
            while Char.IsDigit(lexer.peek()) do
                lexer.curToken.Append(lexer.nextChar()) |> ignore
    let token = lexer.curToken.ToString()
    if Char.IsLetter(lexer.peek()) then
        TK_ERROR(token + string (lexer.peek()), lexer.lineNo)
    else
        if token.Contains(".") then
            if token.EndsWith(".") then
                TK_ERROR("Invalid Numeral: " + token, lexer.lineNo)
            else
                try
                    let floatVal = (lexer.curToken.ToString()) |> Convert.ToSingle
                    TK_REALLIT(floatVal, lexer.lineNo)
                with
                    |_ -> TK_ERROR(token, lexer.lineNo)
        else
            try
                let intVal = (lexer.curToken.ToString()) |> Convert.ToInt32
                TK_INTLIT(intVal, lexer.lineNo)
            with
                |_ -> TK_ERROR(token, lexer.lineNo)

/// Eats whitespace
let eatWhiteSpace (lexer : LexerDriver) (tokList: List<Token>) =
    lexer.curToken.Clear() |> ignore
    while Char.IsWhiteSpace(lexer.peek()) do
        if lexer.curChar() = '\013' && lexer.peek() = '\010' then
            lexer.lineNo <- lexer.lineNo + 1
            lexer.colNo <- 1
        lexer.nextChar() |> ignore
    

/// principal function driving the scanner. Loops through all chars and getTokens
/// takes takes a string path to the file to be compiled as input
/// returns a List<Token> collection as output
let scan (file :string) =
    let charArray = readFileIntoCharArray(file)
    let (lexer:LexerDriver) = {
         chars = charArray 
         pos = -1 
         curToken = new StringBuilder()
         lineNo = 1
         colNo = 0
         lastTok = TK_EMPTYORDEFAULT
        }
    let tokenList = new System.Collections.Generic.List<Token>()
    
    if lexer.chars.Length <= 0 then
        printfn "Empty file. Nothing to parse."
        tokenList.Add(TK_EOF)
        tokenList
    else    // getToken()
        while lexer.nextChar() <> Char.MinValue do
            match lexer.curChar() with 
            |IsLetter -> 
                lexer.lastTok <- (scanAlphaNumericID lexer)
                tokenList.Add(lexer.lastTok)
            |IsDigit -> 
                lexer.lastTok <- (scanNumeric lexer)
                tokenList.Add(lexer.lastTok)
            |IsQuote -> 
                lexer.lastTok <- scanString lexer
                tokenList.Add(lexer.lastTok)
            |IsWhiteSpace -> eatWhiteSpace lexer tokenList
            |IsComment -> eatComments lexer tokenList
            |MaybeComment ->
                if lexer.peek() = '*' then 
                    eatComments lexer tokenList
                else 
                    lexer.lastTok <- (scanSymbolic lexer)
                    tokenList.Add(lexer.lastTok)
            |IsSymbol -> 
                lexer.lastTok <- (scanSymbolic lexer)
                tokenList.Add(lexer.lastTok)

            // if last token matched was an error, stop scanning
            match lexer.lastTok with
            |TK_ERROR(err, _) -> lexer.pos <- lexer.chars.Length // next round of while will fail -> quit scanning
            |_ -> ()
            
        match lexer.lastTok with
        |TK_ERROR(_, _) -> 
            printfn "Scanner Error: at lineNo: %d, colNo: %d. \n" lexer.lineNo (lexer.colNo)
            tokenList
        | _ ->
            printfn "Scan operation was successful. \n"
            tokenList.Add(TK_EOF)   //indicates we've reached EOF w/out any scanner errors
            tokenList
