﻿module ErrorReporting

open System

type ErrorType = 
    |NoError
    |ScannerError
    |ParserError
    |SemanticError
    |CodeGenError

type ErrorReporter = {
    mutable errorState : bool
    mutable errorType : ErrorType
    mutable errorMessage : string Option
}
with
    member this.setError errType errMessage =
        this.errorState <- true
        this.errorType <- errType
        this.errorMessage <- Some errMessage
        printfn"\n%s" errMessage
end