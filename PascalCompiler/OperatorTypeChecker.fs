﻿module OperatorTypeChecker

open Attributes
open NodeTypes
open PascalTokens

let checkBinaryOpType (opNode : ASTNode) arg1 arg2 =
    let operator = Option.get opNode.value 
    match operator with
    |Token.OP_ADD(_) |Token.OP_SUB(_) |Token.OP_MULT(_) ->
        match arg1, arg2 with
        |Some Int, Some Int -> Some Int
        |Some Int, Some Real |Some Real, Some Int |Some Real, Some Real -> Some Real
        |_ -> None
    |Token.OP_REALDIV(_) ->
        match arg1, arg2 with
        |Some Real, Some Real |Some Int, Some Real 
        |Some Real, Some Int |Some Int, Some Int -> Some Real
        |_ -> None
    |Token.OP_INTDIV(_) |Token.OP_INTMOD(_) ->
        match arg1, arg2 with
        |Some Int, Some Int -> Some Int
        |_ -> None
    |Token.OP_AND(_) |Token.OP_OR(_) ->
        match arg1, arg2 with
        |Some Bool, Some Bool -> Some Bool
        |_ -> None
    |Token.OP_LESS(_) |Token.OP_LESSEQ(_)
    |Token.OP_GREAT(_) |Token.OP_GREATEQ(_)
    |Token.OP_EQUAL(_) |Token.OP_NOTEQUAL(_) -> 
        match arg1, arg2 with
        |Some Bool, Some Bool |Some Int, Some Int |Some Real, Some Real
        |Some Int, Some Real |Some Real, Some Int 
        |Some Type.String, Some Type.String -> Some Bool
        |_ -> None
    |_ -> None

let checkUnaryOpType (opNode : ASTNode) arg =  
    let operator = Option.get opNode.value
    match operator with
    |Token.OP_NOT(_) ->
        match arg with
        |Some Bool -> Some Bool
        |_ -> None
    |Token.OP_ADD(_) |Token.OP_SUB(_) ->
        match arg with
        |Some Int -> Some Int
        |Some Real -> Some Real
        |_ -> None
    |_ -> None