﻿module SemanticAnalysis

open System
open System.Linq
open System.Collections.Generic

open NodeTypes
open PascalTokens
open SymbolTable
open Attributes
open ErrorReporting
open OperatorTypeChecker


let rec check_Program (reporter : ErrorReporter) (table : GlobalTable) (node : ASTNode) =
    table.openScope ()
    check_ProgramHeading reporter table node.children.[0]
    check_Block reporter table node.children.[1]
    table.closeScope ()

and check_ProgramHeading reporter table node = 
    () // TO DO

and check_Block reporter table node =
    for child in node.children do
        if not reporter.errorState then
            match child.nodeType with
            |NodeTypes.Declaration_Part |NodeTypes.Local_Declaration_Part -> 
                check_DeclarationPart reporter table child
            |NodeTypes.Statement_Sequence -> check_StatementSequence reporter table child
            |_ -> () // should never match

and check_DeclarationPart reporter table node =
    for child in node.children do
        if not reporter.errorState then
            match child.nodeType with
            |NodeTypes.Constant_Definition_Part -> 
                check_ConstantDefinitionPart reporter table child
            |NodeTypes.Variable_Declaration_Part ->
                check_VariableDeclarationPart reporter table child
            |NodeTypes.Procedure_And_Function_Declaration_Part ->
                check_ProcAndFuncDeclarationPart reporter table child
            |_ -> () // should never match

and check_ConstantDefinitionPart reporter table node =
    for child in node.children do
        check_ConstantDefinition reporter table child

and check_ConstantDefinition reporter table node =
    let idNode = node.children.[0]
    let token = idNode.value |> Option.get // TK_IDENT containing identifier
    let tokenType = check_Constant reporter table node.children.[1] // return attribute
    if not reporter.errorState  then
        node.attribute <- tokenType    //decorate constDefNode with identifier attribute
        idNode.attribute <- tokenType
        table.addSymbol token (tokenType.Value, node) reporter
    
and check_Constant reporter table node =
    let valueIndex =
        match node.children.[0].nodeType with
        |NodeTypes.Sign -> 1
        |_ -> 0
    let typeNode = node.children.[valueIndex]
    match typeNode.nodeType with
    |NodeTypes.KW ->
        match typeNode.value.Value with
        |Token.KW_TRUE(_) |Token.KW_FALSE(_) -> Some (Constant Bool)
        |_ -> Some (Constant Nil)
    |NodeTypes.String -> Some (Constant String)
    |NodeTypes.Number ->
        match typeNode.value.Value with
        |Token.TK_INTLIT(_) -> Some (Constant Int)
        |_ -> Some (Constant Real)
    |NodeTypes.Identifier -> 
        let attrib = table.lookup typeNode.value.Value reporter
        if attrib.IsSome then
            typeNode.attribute <- attrib.Value |> fst |> Some // decorate identifier with lookup result
            typeNode.linkToDeclaration <- attrib.Value |> snd |> Some // link to declration
            match typeNode.attribute.Value with
            |Variable t |Constant t -> Some (Constant t)
            |_ -> 
                reporter.setError SemanticError "Invalid identifier type in RHS of constant definition."
                None
        else 
            reporter.setError SemanticError "Unknown identifier in Constant assignment."
            None
    |_ -> 
        reporter.setError ErrorType.SemanticError "Unable to determine constant type."
        None

and check_VariableDeclarationPart reporter table node =
    for child in node.children do
        check_VariableDeclaration reporter table child

and check_VariableDeclaration reporter table node =
    let typeNode = node.children.[1]
    let varType = check_Type reporter table typeNode    // get type
    if not reporter.errorState then
        node.attribute <- varType   // decorate
        let identifierTokens = get_IdentifierList node.children.[0]
        let identifierNodes = node.children.[0].children // NodeTypes.IdenfierList
        for (token, astNode) in (Seq.zip identifierTokens identifierNodes) do
            if not reporter.errorState then // add ids to table
                astNode.attribute <- varType
                table.addSymbol token (varType.Value, astNode) reporter

and check_Type reporter table node =
    match node.children.[0].nodeType with
    |NodeTypes.Required_Type -> check_RequiredType node.children.[0]
    |NodeTypes.Array_Type -> check_ArrayType reporter table node.children.[0]
    |NodeTypes.Subrange_Type ->
        reporter.setError ErrorType.SemanticError "Sorry: Subrange types not supported." 
        None
    |_ -> 
        reporter.setError ErrorType.SemanticError "Unknown type in variable declaration." 
        None        

and check_RequiredType node =
    match node.value.Value with
    |Token.KW_INTEGER(_) -> Some (Variable Int)
    |Token.KW_REAL(_) -> Some (Variable Real)
    |Token.KW_STRING(_) -> Some (Variable String)
    |Token.KW_BOOLEAN(_) -> Some (Variable Bool)
    |_ -> None // should never match

and check_ArrayType reporter table node =
    let sizeNode = node.children.[0] // expression which must eval to int index
    let indexType = check_Expression reporter table sizeNode
    match indexType with
    |Some (Expression Int) -> ()
    |_ -> reporter.setError SemanticError "Array index expression must evaluate to Int"
    if not reporter.errorState then 
        let elemType = check_RequiredType node.children.[1] // array element type
        match elemType.Value with
        |Variable(t) |Constant(t) -> Some (Array (t))
        |_ -> 
            reporter.setError ErrorType.SemanticError "Could not resolve array element type."
            None
    else None

and get_IdentifierList node =
    node.children
    |> Seq.map (fun child -> child.value.Value)

and check_ProcAndFuncDeclarationPart reporter table node =
    for child in node.children do
        if not reporter.errorState then
            match child.nodeType with
            |NodeTypes.Procedure_Declaration -> check_ProcedureDeclaration reporter table child
            //|NodeTypes.Function_Declaration -> check_FunctionDeclaration reporter table child
            |_ -> // will never match 
                reporter.setError ErrorType.SemanticError "Unmatched proc or func declaration!"

and check_ProcedureDeclaration reporter table node =
    table.openScope ()
    let procedureInfo = check_ProcedureHeading reporter table node.children.[0]
    if not reporter.errorState then
        check_Block reporter table node.children.[1]
        table.closeScope ()
    if not reporter.errorState then
        let typeListOption =
            procedureInfo
            |> snd
            |> Seq.map (fun attrib -> 
                match attrib with
                |Variable t |Constant t -> Some t
                |_ -> None
                )
        if Seq.forall (fun e -> Option.isSome e) typeListOption then
            let typeList =
                typeListOption
                |> Seq.map (fun e -> e.Value)
            let procAttributes = Procedure (ResizeArray<Type>(typeList))
            table.addSymbol (fst procedureInfo) (procAttributes, node) reporter
            node.attribute <- Some procAttributes    
        else reporter.setError SemanticError "Error in procedure argument types."     
    

and check_ProcedureHeading reporter table node =
    let procedureID = node.children.[0].value.Value // extract procedure name
    let procedureAttributes =
        if node.children.Count > 1 then
            check_FormalParameterList reporter table node.children.[1]
        else ResizeArray<Attribute>(0)
    procedureID, procedureAttributes

and check_FormalParameterList reporter table node =
    let paramList = ResizeArray<Attribute>()
    for child in node.children do       // child = formal param section
        let vpsNode = child.children.[0] // vps = value parameter section
        if not reporter.errorState then
            let elemAttribs = check_Type reporter table vpsNode.children.[1] // types of elems in identifier list
            let identifierTokens = get_IdentifierList vpsNode.children.[0]
            let identifierASTNodes = vpsNode.children.[0].children
            for (token, astNode) in (Seq.zip identifierTokens identifierASTNodes) do
                if not reporter.errorState then
                    paramList.Add(elemAttribs.Value)
                    table.addSymbol token (elemAttribs.Value, astNode) reporter
    paramList

and check_StatementSequence reporter table node =
    for child in node.children do
        if not reporter.errorState then
            check_Statement reporter table child

and check_Statement reporter table node =
    if node.children.Count > 0 then // not an empty statement
        let child = node.children.[0]
        let statementType = child.nodeType
        match statementType with
        |NodeTypes.Var_Assignment_Statement -> check_VarAssignmentStatement reporter table child
        |NodeTypes.Array_Assignment_Statement -> check_ArrayAssignmentStatement reporter table child
        |NodeTypes.Procedure_Statement -> check_ProcedureStatement reporter table child
        |NodeTypes.Compound_Statement -> check_CompoundStatement reporter table child
        |NodeTypes.While_Statement -> check_WhileStatement reporter table child
        |NodeTypes.Repeat_Statement -> check_RepeatStatement reporter table child
        |NodeTypes.For_Statement -> check_ForStatement reporter table child
        |NodeTypes.If_Statement -> check_IfStatement reporter table child
        |NodeTypes.Case_Statement -> check_CaseStatement reporter table child
        |_ -> () // empty statement

// ========= Expression checking ==============
and check_Expression (reporter : ErrorReporter) (table: GlobalTable) (node : ASTNode) : Attribute option =
    if not reporter.errorState then
        let type1 = check_SimpleExpression reporter table node.children.[0]
        if node.children.Count = 1  then // no rel op
            node.attribute <- 
                match type1 with
                |Some t -> Some (Expression t)
                |None -> 
                    reporter.setError SemanticError "Expression type check error."
                    None
            node.attribute
        else // rel op present
            let relOpNode = node.children.[1]
            let type2 = check_SimpleExpression reporter table node.children.[2]
            node.attribute <- 
                match (checkBinaryOpType relOpNode type1 type2) with
                |Some t -> Some (Expression t)
                |None -> 
                    reporter.setError SemanticError "Expression type check error."
                    None
            node.attribute
    else None

and check_SimpleExpression reporter table node =
    // helper funcs
    let signPresent =
        match node.children.[0].nodeType with
        |NodeTypes.Sign -> true
        |_ -> false
    let ops, terms =
        if signPresent then
            node.children
            |> Seq.skip 1
            |> Seq.toArray
            |> Array.partition (fun e -> e.nodeType = NodeTypes.Addition_Operator)
        else
            node.children
            |> Seq.toArray
            |> Array.partition (fun e -> e.nodeType = NodeTypes.Addition_Operator)
    if not reporter.errorState then
        let termTypes = Array.map (fun t -> check_Term reporter table t) terms 
        if signPresent then 
            termTypes.[0] <- checkUnaryOpType node.children.[0] termTypes.[0]
        if terms.Length = 1 then 
            node.attribute <-
                match termTypes.[0] with
                |Some t -> Some (Expression t)
                |_ -> 
                    reporter.setError SemanticError "Simple Expression type check error"
                    None
            termTypes.[0]
        else // addition operator present
            let state = termTypes.[0]
            let types = termTypes.[1..(termTypes.Length - 1)]
            let termType = Array.fold2 (fun s op t -> checkBinaryOpType op s t) state ops types
            node.attribute <- 
                match termType with
                |Some t -> Some (Expression t)
                |_ -> 
                    reporter.setError SemanticError "Simple Expression type check error"
                    None
            termType
    else None   

and check_Term reporter table node =
    if not reporter.errorState then
        let ops, factors = 
            node.children
            |> Seq.toArray
            |> Array.partition (fun e -> e.nodeType = NodeTypes.Multiplication_Operator)
        let factorTypes = factors |> Array.map (fun f -> check_Factor reporter table f)
        if factors.Length = 1 then
            node.attribute <-
                match factorTypes.[0] with
                |Some t -> Some (Expression t)
                |None -> 
                    reporter.setError SemanticError "Term type check error."
                    None
            factorTypes.[0]
        else // mult operator present
            let state = factorTypes.[0]
            let types = factorTypes.[1 ..(factorTypes.Length - 1)]
            let termType = Array.fold2 (fun s op t -> checkBinaryOpType op s t) state ops types
            node.attribute <-
                match termType with
                |Some t -> Some (Expression t)
                |_ -> 
                    reporter.setError SemanticError "Term type check error."
                    None
            termType
    else None // error

and check_Factor reporter table node =
    if not reporter.errorState then
        /// node.children.[0] nodeType will decide type of factor
        let factor = node.children.[0]
        match factor.nodeType with
        |NodeTypes.KW ->
            match factor.value.Value with
            |Token.OP_NOT(_) -> // not (expression) case
                let expression = node.children.[1] 
                let factorType = check_Factor reporter table expression
                match factorType with
                |Some Bool -> 
                    factor.attribute <- Some (Expression Bool)
                    node.attribute <- Some (Expression Bool)
                    Some Bool
                |_ -> 
                    reporter.setError SemanticError "Invalid expression type in <Factor> case: NOT <Expression>."
                    None
            |Token.KW_NIL(_) ->
                factor.attribute <- Some (Constant Nil)
                node.attribute <- Some (Expression Nil)
                Some Nil
            |Token.KW_TRUE(_) |Token.KW_FALSE(_) ->
                factor.attribute <- Some (Constant Bool)
                node.attribute <- Some (Expression Bool)
                Some Bool
            |_ ->
                reporter.setError SemanticError "Unknown type in factor"
                None
        |NodeTypes.Identifier ->
            let identifierData = table.lookup factor.value.Value reporter
            if Option.isSome identifierData then
                let identifierType =
                    match (identifierData.Value |> fst) with
                    |Constant t ->
                        factor.attribute <- Some (Constant t)
                        factor.linkToDeclaration <- identifierData.Value |> snd |> Some
                        node.attribute <- Some (Expression t)
                        Some t
                    |Variable t -> 
                        factor.attribute <- Some (Variable t)
                        factor.linkToDeclaration <- identifierData.Value |> snd |> Some
                        node.attribute <- Some (Expression t)
                        Some t
                    |Array (_) -> 
                        reporter.setError SemanticError "Missing index in array reference."
                        None
                    |_ -> 
                        reporter.setError SemanticError "Unknown identifier type in <factor>"
                        None
                if not reporter.errorState then
                    let linkToDeclaration = identifierData.Value |> snd
                    factor.attribute <- identifierData |> Option.get |> fst |> Some
                    factor.linkToDeclaration <- linkToDeclaration |> Some
                    node.attribute <- Some (Expression identifierType.Value)
                    identifierType
                else None
            else None
        |NodeTypes.Array_Variable ->
            let identifierNode = factor.children.[0]
            let arrayData = table.lookup identifierNode.value.Value reporter
            if Option.isSome arrayData then
                let arrayType = 
                    match arrayData.Value |> fst with
                    |Array t -> Some t
                    |_ -> None
                let indexNode = factor.children.[1]
                let indexType = 
                    match (check_Expression reporter table indexNode) with
                    |Some (Expression Int) -> Some Int
                    |_ -> 
                        reporter.setError SemanticError "Non-integer index in Array variable."
                        None
                if arrayType.IsSome && indexType.IsSome then
                    identifierNode.attribute <- arrayData |> Option.get |> fst |> Some
                    identifierNode.linkToDeclaration <- arrayData |> Option.get |> snd |> Some
                    factor.attribute <- Some (Expression arrayType.Value)
                    node.attribute <- factor.attribute
                    arrayType
                else None
            else None
        |NodeTypes.Number ->
            match factor.value.Value with
            |Token.TK_INTLIT(_) ->
                factor.attribute <- Some (Constant Int)
                node.attribute <- Some (Expression Int)
                Some Int
            |Token.TK_REALLIT(_) ->
                factor.attribute <- Some (Constant Real)
                node.attribute <- Some (Expression Real)
                Some Real
            |_ ->
                reporter.setError SemanticError "Unknown number type in factor"
                None
        |NodeTypes.String ->
                factor.attribute <- Some (Constant String)
                node.attribute <- Some (Expression String)
                Some String
        |NodeTypes.Expression ->
            let exprAttrib = check_Expression reporter table factor
            factor.attribute <- exprAttrib
            node.attribute <- exprAttrib
            match exprAttrib with
            |Some (Expression t) -> 
                node.attribute <- exprAttrib
                Some t
            |_ -> 
                reporter.setError SemanticError "Invalid expression type in <Factor> case: (<Expression>)."
                None
        |_ ->
            reporter.setError SemanticError "Unknown factor case"
            None
    else None

and check_VarAssignmentStatement reporter table node = 
    if not reporter.errorState then
        let lhsNode = node.children.[0] // identifier
        let rhsNode = node.children.[1] // expression
        let identifierInfo = table.lookup lhsNode.value.Value reporter
        let expressionInfo = check_Expression reporter table rhsNode
        if identifierInfo.IsSome && expressionInfo.IsSome then
            lhsNode.attribute <- identifierInfo |> Option.get |> fst |> Some
            lhsNode.linkToDeclaration <- identifierInfo |> Option.get |> snd |> Some
            match lhsNode.attribute.Value, expressionInfo.Value with
            |Variable t1, Expression t2  when t1 = t2 -> () // all is fine
            |Array t1, Expression t2 when t1 = t2 -> 
                reporter.setError SemanticError "Missing index in array assignment statement."
            |Variable Int, Expression Real |Variable Real, Expression Int -> () // cast
            |Constant _, _ -> reporter.setError SemanticError "Cannot assign to a constant."
            |_ -> reporter.setError SemanticError "Mismatched types in LHS and RHS of assignment"
        else
            reporter.setError SemanticError "Error in checking of Assignment statement"

and check_ArrayAssignmentStatement reporter table node =
    if not reporter.errorState then
        let lhsNode = node.children.[0] // array variable: identifier[index]
        let rhsNode = node.children.[1] // expression
        let lhsType = check_ArrayVariable reporter table lhsNode // array element type
        let rhsInfo = check_Expression reporter table rhsNode // expression attribute
        if Option.isSome lhsType && Option.isSome rhsInfo then
            match lhsType.Value, rhsInfo.Value with
            |t1, Expression t2 when t1 = t2 -> () // everything is fine
            |Int, Expression Real |Real, Expression Int -> () // cast between int and real
            |_ -> reporter.setError SemanticError "Type mismatch in array assignment statement."
        else reporter.setError SemanticError "Type Error in Array assignment statement."

and check_ArrayVariable reporter table node =
    if not reporter.errorState then
        let identifierNode = node.children.[0]
        let indexNode = node.children.[1]
        let identifierInfo = table.lookup identifierNode.value.Value reporter
        let indexInfo = check_Expression reporter table indexNode
        if identifierInfo.IsSome && indexInfo.IsSome then
            identifierNode.attribute <- identifierInfo |> Option.get |> fst |> Some
            identifierNode.linkToDeclaration <- identifierInfo |> Option.get |> snd |> Some
            match identifierNode.attribute.Value , indexInfo.Value with
            |Array t, Expression Int -> Some t
            |_ -> // expression did not evaluate to Int
                reporter.setError SemanticError "Index expression did not evaluate to Int"
                None
        else
            reporter.setError SemanticError "Type Error while checking Array Variable."
            None
    else None

and check_ProcedureStatement reporter table node =
    let isSystemProc attrib =
        match attrib with
        |SysProc (_) -> true
        |_ -> false
    if not reporter.errorState then
        let identifierNode = node.children.[0]
        let identifierInfo = table.lookup identifierNode.value.Value reporter
        if Option.isSome identifierInfo then // proc found in sym table
            let procFormalParamTypes = 
                match (identifierInfo |> Option.get |> fst) with
                |Procedure p -> p
                |_ -> ResizeArray<Type>(0)
            let procActualParamTypes =  // expressions
                node.children 
                |> Seq.skip 1 // skip identifier node
                |> Seq.map (check_Expression reporter table)
                |> Seq.map (
                    function
                    |Some (Expression t) -> Some t
                    |_ -> None)
            if not reporter.errorState && 
                Seq.forall (fun t -> Option.isSome t) procActualParamTypes then 
                

                // TODO: if-cond to check for sysProc
                if isSystemProc (identifierInfo.Value |> fst) then
                    identifierNode.attribute <- identifierInfo.Value |> fst |> Some
                    identifierNode.linkToDeclaration <- identifierInfo.Value |> snd |> Some
                
                // check that number or arguments match: not sys proc
                elif not (Seq.length procActualParamTypes = Seq.length procFormalParamTypes) then
                    reporter.setError SemanticError "Procedure call with wrong number of arguments."
                // check that argument types match
                elif not (Seq.forall2 (fun t1 t2 -> t1 = (Option.get t2)) procFormalParamTypes procActualParamTypes) then
                    reporter.setError SemanticError "Mismatched arguments in proc call"
                else // all checks for user defined proc passed -> decorate identifierNode
                    identifierNode.attribute <- identifierInfo.Value |> fst |> Some
                    identifierNode.linkToDeclaration <- identifierInfo.Value |> snd |> Some
            else reporter.setError SemanticError "Error in Procedure arguents" 
        else reporter.setError SemanticError "Undeclared procedure call."

and check_CompoundStatement reporter table node =
    check_StatementSequence reporter table node.children.[0]

and check_WhileStatement reporter table node =
    if not reporter.errorState then
        let conditionNode = node.children.[0]
        let statementNode = node.children.[1]
        let exprAttrib = check_Expression reporter table conditionNode
        match exprAttrib with
        |Some (Expression Bool) -> check_Statement reporter table statementNode
        |_ -> reporter.setError SemanticError "Expecting bool condition in <while> statement."

and check_RepeatStatement reporter table node =
    if not reporter.errorState then
        let conditionNode = node.children.[1]
        let statementNode = node.children.[0]
        let exprAttrib = check_Expression reporter table conditionNode
        match exprAttrib with
        |Some (Expression Bool) -> check_StatementSequence reporter table statementNode
        |_ -> reporter.setError SemanticError "Expecting bool condition in <repeat> statement."

and check_IfStatement reporter table node =
    if not reporter.errorState then    
        let conditionNode = node.children.[0]
        let ifClauseNode = node.children.[1]
        let exprAttrib = check_Expression reporter table conditionNode
        match exprAttrib with
        |Some (Expression Bool) -> check_Statement reporter table ifClauseNode
        |_ -> reporter.setError SemanticError "Expecting bool condition in <if> statement."
        if not reporter.errorState then
            match node.children.Count with
            |3 -> check_Statement reporter table node.children.[2] // check else clause
            |_ -> ()

and check_ForStatement reporter table node =
    if not reporter.errorState then
        let identifierNode = node.children.[0]
        let initBoundNode = node.children.[1]
        let finalBoundNode = node.children.[3]
        let statementNode = node.children.[4]
        let identifierInfo = table.lookup identifierNode.value.Value reporter
        if not reporter.errorState then // check that identifier was declared and of type int
            identifierNode.attribute <- identifierInfo.Value |> fst |> Some
            identifierNode.linkToDeclaration <- identifierInfo.Value |> snd |> Some
            match identifierNode.attribute with
            |Some (Variable Int) -> ()
            |_ -> reporter.setError SemanticError "Invalid bound identifier type In <for> statement."
        if not reporter.errorState then // check bound expressions
            let initBoundInfo = check_Expression reporter table initBoundNode
            let finalBoundInfo = check_Expression reporter table finalBoundNode
            match initBoundInfo, finalBoundInfo with
            |Some (Expression Int), Some (Expression Int) -> ()
            |_ -> reporter.setError SemanticError "Invalid bound expressions in <for> statement."
        if not reporter.errorState then
            check_Statement reporter table statementNode

and check_CaseStatement reporter table node =
    if not reporter.errorState then
        let expressionNode = node.children.[0]
        let caseLimbNodes = node.children |> Seq.skip 1
        let expressionInfo = check_Expression reporter table expressionNode
        for case in caseLimbNodes do
            if not reporter.errorState then
                check_CaseLimb reporter table expressionInfo case

and check_CaseLimb reporter table expressionInfo node =
    let labelListNode = node.children.[0]
    let statementNode = node.children.[1]
    let labelTypes = check_CaseLabelList reporter table labelListNode // seq of label types
    for constType in labelTypes do
        if not reporter.errorState then
            match constType, expressionInfo with
            |Some (Constant t1), Some (Expression t2) when t1 = t2 -> ()
            |_ -> reporter.setError SemanticError "Constant case type does not match expression type."
    if not reporter.errorState then
        check_Statement reporter table statementNode

and check_CaseLabelList reporter table node =
    seq {for n in node.children -> check_Constant reporter table n}


/// do the semantic check for the entire program
let semanticCheck node =
    let errorReporter = {
        errorState = false
        errorType = NoError
        errorMessage = None
    }
    let symbolTable = {
        tableStack = Stack<Table>()
        maxScope = -1
    }

    symbolTable.loadStandardEnvironment errorReporter
    check_Program errorReporter symbolTable node
    if errorReporter.errorState then
        None
    else Some node
