﻿(*
    This file/module defines the Token discriminated union
    which classifies the token types supported by our compiler
    e.g.: keywords (KW_), operators (OP_), symbols (SYM_), etc...
*)

module PascalTokens

type Token =
    |KW_ARRAY of int
    |KW_BEGIN of int
    |KW_BOOLEAN of int
    |KW_CASE of int
    |KW_CHARACTER of int
    |KW_CONST of int
    |KW_DO of int
    |KW_DOWNTO of int
    |KW_ELSE of int
    |KW_END of int
    |KW_FALSE of int
    |KW_FILE of int
    |KW_FOR of int
    |KW_FUNCTION of int
    |KW_GOTO of int
    |KW_IF of int
    |KW_INTEGER of int
    |KW_LABEL of int
    |KW_NIL of int
    |KW_OF of int
    |KW_PACKED of int
    |KW_PROCEDURE of int
    |KW_PROGRAM of int
    |KW_REAL of int
    |KW_RECORD of int
    |KW_REPEAT of int
    |KW_STRING of int
    |KW_SET of int
    |KW_THEN of int
    |KW_TO of int
    |KW_TRUE of int
    |KW_TYPE of int
    |KW_UNTIL of int
    |KW_VAR of int
    |KW_WHILE of int
    |KW_WITH of int
    |OP_INTDIV of int
    |OP_REALDIV of int
    |OP_ADD of int
    |OP_SUB of int
    |OP_MULT of int
    |OP_INTMOD of int
    |OP_ASSIGN of int
    |OP_EQUAL of int
    |OP_NOTEQUAL of int
    |OP_LESS of int
    |OP_GREAT of int
    |OP_LESSEQ of int
    |OP_GREATEQ of int
    |OP_IN of int
    |OP_NOT of int
    |OP_OR of int
    |OP_AND of int
    |OP_XOR of int
    |OP_SHL of int
    |OP_SHR of int
    |SYM_LPAREN of int
    |SYM_RPAREN of int
    |SYM_LBRACKET of int
    |SYM_RBRACKET of int
    |SYM_CARET of int
    |SYM_AT of int
    |SYM_COMMA of int
    |SYM_DOT of int
    |SYM_DOTDOT of int
    |SYM_COLON of int
    |SYM_SEMICOLON of int
    |TK_IDENT of string * int
    |TK_INTLIT of int * int
    |TK_REALLIT of float32 * int
    |TK_STRLIT of string * int
    |TK_ERROR of string * int
    |TK_EOL 
    |TK_EOF
    |TK_EMPTYORDEFAULT