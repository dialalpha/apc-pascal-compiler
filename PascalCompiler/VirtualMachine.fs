﻿module VirtualMachine

open System.Collections.Generic

open NodeTypes
open PascalTokens

type Data =
    |Int of int
    |Real of single
    |Bool of bool
    |String of string
    |Nil
    |Array of Data[]

type Interpreter = {
    dataStore : Data[]
    FP : Stack<int> // frame pointers
    mutable SB : int
    mutable LB : int
    mutable ST : int
    mutable inLocalFrame : bool
}
with
    member this.pushImmediate data = 
        this.dataStore.[this.ST] <- data
        this.ST <- this.ST + 1
    
    member this.allocateVar () =
        this.ST <- this.ST + 1

    member this.assignPrimitive (r, d) data =
        if this.LB + d > this.ST then
            failwith "Runtime error: Stack overflow detected."
        match r with
        |0 -> this.dataStore.[d] <- data
        |_ -> this.dataStore.[this.LB + d] <- data

    member this.assignArray (r, d) i data =
        if this.LB + d > this.ST then
            failwith "Runtime error: Stack overflow detected."
        match r with
        |0 -> 
            match this.dataStore.[d] with
            |Array arr -> arr.[i] <- data
            |_ -> failwith "Attempt to assign into non-array index."
        |_ -> 
            match this.dataStore.[this.LB + d] with
            |Array arr -> arr.[i] <- data
            |_ -> failwith "Attemmpt to assign into non-array index."
    
    member this.readPrimitive (r, d) = // register and displacement
        if this.LB + d > this.ST then
            failwith "Runtime error: Stack overflow detected."
        match r with
        |0 -> this.dataStore.[d]
        |_ -> this.dataStore.[this.LB + d]

    member this.readArray (r, d) i =
        if this.LB + d > this.ST then
            failwith "Runtime error: Stack overflow detected."
        match r with
        |0 ->
            match this.dataStore.[d] with
            |Array arr -> arr.[i]
            |_ -> failwith "Attempt to index into non-array value."
        |_ ->
            match this.dataStore.[this.LB + d] with
            |Array arr -> arr.[i]
            |_ -> failwith "Attempt to index into non-array value."

    member this.pushStackFrame () =
        this.inLocalFrame <- true
        this.FP.Push(this.LB)
        this.LB <- this.ST

    member this.popStackFrame () =
        this.ST <- this.LB
        this.LB <- this.FP.Pop()
        if this.FP.Count = 0 then this.inLocalFrame <- false
end


let eval_binaryOp (opNode : ASTNode) arg1 arg2 =
    let operator = opNode.value |> Option.get
    match operator with
    |Token.OP_ADD(_) ->
        match arg1, arg2 with
        |Int x, Int y -> Int (x + y)
        |Real x, Real y -> Real (x + y)
        |Int x, Real y -> Real (single x + y) 
        |Real x, Int y -> Real (x + single y)
        |_ -> failwith "Runtime Error: Attempt to add incompatible types."
    |Token.OP_SUB(_) ->
        match arg1, arg2 with
        |Int x, Int y -> Int (x - y)
        |Real x, Real y -> Real (x - y)
        |Int x, Real y -> Real (single x - y) 
        |Real x, Int y -> Real (x - single y)
        |_ -> failwith "Runtime Error: Attempt to subtract incompatible types."
    |Token.OP_MULT(_) ->
        match arg1, arg2 with
        |Int x, Int y -> Int (x * y)
        |Real x, Real y -> Real (x * y)
        |Int x, Real y -> Real (single x * y)
        |Real x, Int y -> Real (x * single y)
        |_ -> failwith "Runtime Error: Attempt to multiply incompatible types."
    |Token.OP_INTMOD(_) ->
        match arg1, arg2 with
        |Int x, Int y -> Int (x % y)
        |_ -> failwith "Runtime Error: <mod> operands must both be of type integer."
    |Token.OP_INTDIV(_) ->
        match arg1, arg2 with
        |Int x, Int y -> Int (x / y)
        |_ -> failwith "Runtime Error: <div> operands must both be of type integer."
    |Token.OP_REALDIV(_) ->
        match arg1, arg2 with
        |Int x, Int y -> Real (single x / single y)
        |Int x, Real y -> Real (single x / y)
        |Real x, Int y -> Real (x / single y)
        |Real x, Real y -> Real (x / y)
        |_ -> failwith "Runtime Error: Attempt to divide incompatible types."
    |Token.OP_LESS(_) ->
        match arg1, arg2 with
        |Int x, Int y -> Bool (x < y)
        |Int x, Real y -> Bool (single x < y)
        |Real x, Int y -> Bool (x < single y)
        |Real x, Real y -> Bool (x < y)
        |Bool x, Bool y -> Bool (x < y)
        |String x, String y -> Bool (x < y)
        |_ -> failwith "Runtime Error: Attempt to compare incompatible types."
    |Token.OP_GREAT _ ->
        match arg1, arg2 with
        |Int x, Int y -> Bool (x > y)
        |Int x, Real y -> Bool (single x > y)
        |Real x, Int y -> Bool (x > single y)
        |Real x, Real y -> Bool (x > y)
        |Bool x, Bool y -> Bool (x > y)
        |String x, String y -> Bool (x > y)
        |_ -> failwith "Runtime Error: Attempt to compare incompatible types."
    |Token.OP_GREATEQ _ ->
        match arg1, arg2 with
        |Int x, Int y -> Bool (x >= y)
        |Int x, Real y -> Bool (single x >= y)
        |Real x, Int y -> Bool (x >= single y)
        |Real x, Real y -> Bool (x >= y)
        |Bool x, Bool y -> Bool (x >= y)
        |String x, String y -> Bool (x <= y)
        |_ -> failwith "Runtime Error: Attempt to compare incompatible types."
    |Token.OP_LESSEQ _ ->
        match arg1, arg2 with
        |Int x, Int y -> Bool (x <= y)
        |Int x, Real y -> Bool (single x <= y)
        |Real x, Int y -> Bool (x <= single y)
        |Real x, Real y -> Bool (x <= y)
        |Bool x, Bool y -> Bool (x <= y)
        |String x, String y -> Bool (x >= y)
        |_ -> failwith "Runtime Error: Attempt to compare incompatible types."
    |Token.OP_EQUAL _ ->
        match arg1, arg2 with
        |Int x, Int y -> Bool (x = y)
        |Int x, Real y -> Bool (single x = y)
        |Real x, Int y -> Bool (x = single y)
        |Real x, Real y -> Bool (x = y)
        |Bool x, Bool y -> Bool (x = y)
        |String x, String y -> Bool (x = y)
        |_ -> failwith "Runtime Error: Attempt to compare incompatible types."
    |Token.OP_NOTEQUAL _ ->
        match arg1, arg2 with
        |Int x, Int y -> Bool (x <> y)
        |Int x, Real y -> Bool (single x <> y)
        |Real x, Int y -> Bool (x <> single y)
        |Real x, Real y -> Bool (x <> y)
        |Bool x, Bool y -> Bool (x <> y)
        |String x, String y -> Bool (x <> y)
        |_ -> failwith "Runtime Error: Attempt to compare incompatible types."
    |Token.OP_OR _ ->
        match arg1, arg2 with
        |Bool x, Bool y -> Bool (x || y)
        |_ -> failwith "Runtime Error: Invalid arg types in <or> expression."
    |Token.OP_AND _ ->
        match arg1, arg2 with
        |Bool x, Bool y -> Bool (x && y)
        |_ -> failwith "Runtime Error: Invalid arg types in <and> expression."
    |_ -> failwith "Runtime Error: Unknown binary operation."

let eval_unaryOp (opNode : ASTNode) arg =
    let operator = opNode.value |> Option.get
    match operator with
    |Token.OP_NOT _ ->
        match arg with
        |Bool x -> Bool (not x)
        |_ -> failwith "Runtime Error: Invalid type in boolean <not> expression."
    |Token.OP_ADD _ ->
        match arg with
        |Int x -> Int x
        |Real x -> Real x
        |_ -> failwith "Runtime Error: Invalid arg in unary <+> op"
    |Token.OP_SUB _ ->
        match arg with
        |Int x -> Int -x
        |Real x -> Real -x
        |_ -> failwith "Runtime Error: Invalid arg in unary <-> op"
    |_ -> failwith "Runtime Error: Unknown unary operator."

