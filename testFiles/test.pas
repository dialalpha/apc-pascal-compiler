program test;
var
	a:Integer;
	b:Real;
	arr : array[10] of Integer;
	
procedure assignAndprint(a: integer);
	var 
		b:Integer;
	begin
		b := 20;
		a := b * 7;
		writeln('updated "a" inside procedure call');
		writeln('a: ', a);
	end;
begin
	
	writeln('testing expressions:');
	writeln('-1 + 3 -> ', -1 + 3);
	writeln('not true ->', not true);
	writeln('1 < 2.0 -> ', 1 < 2.0);
	writeln('a = a -> ', 'a' = 'a');
	writeln('2 + 3 * (4 -1)', 2 + 3 * (4 - 1));
	
	writeln;
	writeln('Testing assignments');
	a := 2;
	b := 2.5 + a;
	arr[0] := 0;
	arr[9] := 9;
	arr[5] := arr[9];
	writeln('a: ', a);
	assignAndPrint(a);
	writeln('b: ' ,b);
	writeln('arr[0]: ', arr[0]);
	writeln('arr[9]: ', arr[9]);
	writeln('arr[5]: ', arr[5]);
	
	(*for loop*)
	writeln;
	writeln('Testing for loop');
	for a := 1 to 10 do
		writeln(a);
		
	(*while loop*)
	writeln;
	writeln('testing while loop');
	
	while a >= 0 do
		begin
			writeln(a);
			a := a - 1;
		end;
	
	(*case*)
	writeln;
	case 'alpha' of
		'vulis': writeln('vulis');
		'mike','chris': writeln('mike','chris');
		'alpha': writeln ('Case test success: alpha');
		end;
		
	(*repeat*)
	writeln;
	writeln('testing repeat 1 - 5');
	a := 1;
	repeat
		writeln(a);
		a := a + 1;
	until a > 5;
	
	(*if*)
	writeln;
	writeln('testing if');
	if (1 < 2) then
		begin
			writeln('positive if test condition');
		end
	else 
		begin
		writeln ('if test failed');
		end;
	
	
	if 1 > 2 * 45 then
		begin
			writeln('negative if test condition - test failed');
		end
	else 
		begin
			writeln('2nd if test passed.');
		end;
end.